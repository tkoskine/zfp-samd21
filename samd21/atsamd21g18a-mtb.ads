--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.MTB is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype MTB_POSITION_WRAP_Field is ATSAMD21G18A.Bit;
   subtype MTB_POSITION_POINTER_Field is ATSAMD21G18A.UInt29;

   --  MTB Position
   type MTB_POSITION_Register is record
      --  unspecified
      Reserved_0_1 : ATSAMD21G18A.UInt2 := 16#0#;
      --  Pointer Value Wraps
      WRAP         : MTB_POSITION_WRAP_Field := 16#0#;
      --  Trace Packet Location Pointer
      POINTER      : MTB_POSITION_POINTER_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for MTB_POSITION_Register use record
      Reserved_0_1 at 0 range 0 .. 1;
      WRAP         at 0 range 2 .. 2;
      POINTER      at 0 range 3 .. 31;
   end record;

   subtype MTB_MASTER_MASK_Field is ATSAMD21G18A.UInt5;
   subtype MTB_MASTER_TSTARTEN_Field is ATSAMD21G18A.Bit;
   subtype MTB_MASTER_TSTOPEN_Field is ATSAMD21G18A.Bit;
   subtype MTB_MASTER_SFRWPRIV_Field is ATSAMD21G18A.Bit;
   subtype MTB_MASTER_RAMPRIV_Field is ATSAMD21G18A.Bit;
   subtype MTB_MASTER_HALTREQ_Field is ATSAMD21G18A.Bit;
   subtype MTB_MASTER_EN_Field is ATSAMD21G18A.Bit;

   --  MTB Master
   type MTB_MASTER_Register is record
      --  Maximum Value of the Trace Buffer in SRAM
      MASK           : MTB_MASTER_MASK_Field := 16#0#;
      --  Trace Start Input Enable
      TSTARTEN       : MTB_MASTER_TSTARTEN_Field := 16#0#;
      --  Trace Stop Input Enable
      TSTOPEN        : MTB_MASTER_TSTOPEN_Field := 16#0#;
      --  Special Function Register Write Privilege
      SFRWPRIV       : MTB_MASTER_SFRWPRIV_Field := 16#0#;
      --  SRAM Privilege
      RAMPRIV        : MTB_MASTER_RAMPRIV_Field := 16#0#;
      --  Halt Request
      HALTREQ        : MTB_MASTER_HALTREQ_Field := 16#0#;
      --  unspecified
      Reserved_10_30 : ATSAMD21G18A.UInt21 := 16#0#;
      --  Main Trace Enable
      EN             : MTB_MASTER_EN_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for MTB_MASTER_Register use record
      MASK           at 0 range 0 .. 4;
      TSTARTEN       at 0 range 5 .. 5;
      TSTOPEN        at 0 range 6 .. 6;
      SFRWPRIV       at 0 range 7 .. 7;
      RAMPRIV        at 0 range 8 .. 8;
      HALTREQ        at 0 range 9 .. 9;
      Reserved_10_30 at 0 range 10 .. 30;
      EN             at 0 range 31 .. 31;
   end record;

   subtype MTB_FLOW_AUTOSTOP_Field is ATSAMD21G18A.Bit;
   subtype MTB_FLOW_AUTOHALT_Field is ATSAMD21G18A.Bit;
   subtype MTB_FLOW_WATERMARK_Field is ATSAMD21G18A.UInt29;

   --  MTB Flow
   type MTB_FLOW_Register is record
      --  Auto Stop Tracing
      AUTOSTOP     : MTB_FLOW_AUTOSTOP_Field := 16#0#;
      --  Auto Halt Request
      AUTOHALT     : MTB_FLOW_AUTOHALT_Field := 16#0#;
      --  unspecified
      Reserved_2_2 : ATSAMD21G18A.Bit := 16#0#;
      --  Watermark value
      WATERMARK    : MTB_FLOW_WATERMARK_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for MTB_FLOW_Register use record
      AUTOSTOP     at 0 range 0 .. 0;
      AUTOHALT     at 0 range 1 .. 1;
      Reserved_2_2 at 0 range 2 .. 2;
      WATERMARK    at 0 range 3 .. 31;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  Cortex-M0+ Micro-Trace Buffer
   type MTB_Peripheral is record
      --  MTB Position
      POSITION   : aliased MTB_POSITION_Register;
      --  MTB Master
      MASTER     : aliased MTB_MASTER_Register;
      --  MTB Flow
      FLOW       : aliased MTB_FLOW_Register;
      --  MTB Base
      BASE       : aliased ATSAMD21G18A.UInt32;
      --  MTB Integration Mode Control
      ITCTRL     : aliased ATSAMD21G18A.UInt32;
      --  MTB Claim Set
      CLAIMSET   : aliased ATSAMD21G18A.UInt32;
      --  MTB Claim Clear
      CLAIMCLR   : aliased ATSAMD21G18A.UInt32;
      --  MTB Lock Access
      LOCKACCESS : aliased ATSAMD21G18A.UInt32;
      --  MTB Lock Status
      LOCKSTATUS : aliased ATSAMD21G18A.UInt32;
      --  MTB Authentication Status
      AUTHSTATUS : aliased ATSAMD21G18A.UInt32;
      --  MTB Device Architecture
      DEVARCH    : aliased ATSAMD21G18A.UInt32;
      --  MTB Device Configuration
      DEVID      : aliased ATSAMD21G18A.UInt32;
      --  MTB Device Type
      DEVTYPE    : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID4       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID5       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID6       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID7       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID0       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID1       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID2       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      PID3       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      CID0       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      CID1       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      CID2       : aliased ATSAMD21G18A.UInt32;
      --  CoreSight
      CID3       : aliased ATSAMD21G18A.UInt32;
   end record
     with Volatile;

   for MTB_Peripheral use record
      POSITION   at 16#0# range 0 .. 31;
      MASTER     at 16#4# range 0 .. 31;
      FLOW       at 16#8# range 0 .. 31;
      BASE       at 16#C# range 0 .. 31;
      ITCTRL     at 16#F00# range 0 .. 31;
      CLAIMSET   at 16#FA0# range 0 .. 31;
      CLAIMCLR   at 16#FA4# range 0 .. 31;
      LOCKACCESS at 16#FB0# range 0 .. 31;
      LOCKSTATUS at 16#FB4# range 0 .. 31;
      AUTHSTATUS at 16#FB8# range 0 .. 31;
      DEVARCH    at 16#FBC# range 0 .. 31;
      DEVID      at 16#FC8# range 0 .. 31;
      DEVTYPE    at 16#FCC# range 0 .. 31;
      PID4       at 16#FD0# range 0 .. 31;
      PID5       at 16#FD4# range 0 .. 31;
      PID6       at 16#FD8# range 0 .. 31;
      PID7       at 16#FDC# range 0 .. 31;
      PID0       at 16#FE0# range 0 .. 31;
      PID1       at 16#FE4# range 0 .. 31;
      PID2       at 16#FE8# range 0 .. 31;
      PID3       at 16#FEC# range 0 .. 31;
      CID0       at 16#FF0# range 0 .. 31;
      CID1       at 16#FF4# range 0 .. 31;
      CID2       at 16#FF8# range 0 .. 31;
      CID3       at 16#FFC# range 0 .. 31;
   end record;

   --  Cortex-M0+ Micro-Trace Buffer
   MTB_Periph : aliased MTB_Peripheral
     with Import, Address => System'To_Address (16#41006000#);

end ATSAMD21G18A.MTB;
