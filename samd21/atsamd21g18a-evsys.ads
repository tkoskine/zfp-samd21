--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.EVSYS is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype EVSYS_CTRL_SWRST_Field is ATSAMD21G18A.Bit;
   subtype EVSYS_CTRL_GCLKREQ_Field is ATSAMD21G18A.Bit;

   --  Control
   type EVSYS_CTRL_Register is record
      --  Write-only. Software Reset
      SWRST        : EVSYS_CTRL_SWRST_Field := 16#0#;
      --  unspecified
      Reserved_1_3 : ATSAMD21G18A.UInt3 := 16#0#;
      --  Write-only. Generic Clock Requests
      GCLKREQ      : EVSYS_CTRL_GCLKREQ_Field := 16#0#;
      --  unspecified
      Reserved_5_7 : ATSAMD21G18A.UInt3 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for EVSYS_CTRL_Register use record
      SWRST        at 0 range 0 .. 0;
      Reserved_1_3 at 0 range 1 .. 3;
      GCLKREQ      at 0 range 4 .. 4;
      Reserved_5_7 at 0 range 5 .. 7;
   end record;

   subtype EVSYS_CHANNEL_CHANNEL_Field is ATSAMD21G18A.UInt4;
   subtype EVSYS_CHANNEL_SWEVT_Field is ATSAMD21G18A.Bit;
   subtype EVSYS_CHANNEL_EVGEN_Field is ATSAMD21G18A.UInt7;

   --  Path Selection
   type CHANNEL_PATHSelect is
     (
      --  Synchronous path
      Synchronous,
      --  Resynchronized path
      Resynchronized,
      --  Asynchronous path
      Asynchronous)
     with Size => 2;
   for CHANNEL_PATHSelect use
     (Synchronous => 0,
      Resynchronized => 1,
      Asynchronous => 2);

   --  Edge Detection Selection
   type CHANNEL_EDGSELSelect is
     (
      --  No event output when using the resynchronized or synchronous path
      No_Evt_Output,
      --  Event detection only on the rising edge of the signal from the event
      --  generator when using the resynchronized or synchronous path
      Rising_Edge,
      --  Event detection only on the falling edge of the signal from the event
      --  generator when using the resynchronized or synchronous path
      Falling_Edge,
      --  Event detection on rising and falling edges of the signal from the
      --  event generator when using the resynchronized or synchronous path
      Both_Edges)
     with Size => 2;
   for CHANNEL_EDGSELSelect use
     (No_Evt_Output => 0,
      Rising_Edge => 1,
      Falling_Edge => 2,
      Both_Edges => 3);

   --  Channel
   type EVSYS_CHANNEL_Register is record
      --  Channel Selection
      CHANNEL        : EVSYS_CHANNEL_CHANNEL_Field := 16#0#;
      --  unspecified
      Reserved_4_7   : ATSAMD21G18A.UInt4 := 16#0#;
      --  Software Event
      SWEVT          : EVSYS_CHANNEL_SWEVT_Field := 16#0#;
      --  unspecified
      Reserved_9_15  : ATSAMD21G18A.UInt7 := 16#0#;
      --  Event Generator Selection
      EVGEN          : EVSYS_CHANNEL_EVGEN_Field := 16#0#;
      --  unspecified
      Reserved_23_23 : ATSAMD21G18A.Bit := 16#0#;
      --  Path Selection
      PATH           : CHANNEL_PATHSelect := ATSAMD21G18A.EVSYS.Synchronous;
      --  Edge Detection Selection
      EDGSEL         : CHANNEL_EDGSELSelect :=
                        ATSAMD21G18A.EVSYS.No_Evt_Output;
      --  unspecified
      Reserved_28_31 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EVSYS_CHANNEL_Register use record
      CHANNEL        at 0 range 0 .. 3;
      Reserved_4_7   at 0 range 4 .. 7;
      SWEVT          at 0 range 8 .. 8;
      Reserved_9_15  at 0 range 9 .. 15;
      EVGEN          at 0 range 16 .. 22;
      Reserved_23_23 at 0 range 23 .. 23;
      PATH           at 0 range 24 .. 25;
      EDGSEL         at 0 range 26 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   subtype EVSYS_USER_USER_Field is ATSAMD21G18A.UInt5;

   --  Channel Event Selection
   type USER_CHANNELSelect is
     (
      --  No Channel Output Selected
      USER_CHANNELSelect_0)
     with Size => 5;
   for USER_CHANNELSelect use
     (USER_CHANNELSelect_0 => 0);

   --  User Multiplexer
   type EVSYS_USER_Register is record
      --  User Multiplexer Selection
      USER           : EVSYS_USER_USER_Field := 16#0#;
      --  unspecified
      Reserved_5_7   : ATSAMD21G18A.UInt3 := 16#0#;
      --  Channel Event Selection
      CHANNEL        : USER_CHANNELSelect :=
                        ATSAMD21G18A.EVSYS.USER_CHANNELSelect_0;
      --  unspecified
      Reserved_13_15 : ATSAMD21G18A.UInt3 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 16,
          Bit_Order => System.Low_Order_First;

   for EVSYS_USER_Register use record
      USER           at 0 range 0 .. 4;
      Reserved_5_7   at 0 range 5 .. 7;
      CHANNEL        at 0 range 8 .. 12;
      Reserved_13_15 at 0 range 13 .. 15;
   end record;

   --  EVSYS_CHSTATUS_USRRDY array element
   subtype EVSYS_CHSTATUS_USRRDY_Element is ATSAMD21G18A.Bit;

   --  EVSYS_CHSTATUS_USRRDY array
   type EVSYS_CHSTATUS_USRRDY_Field_Array is array (0 .. 7)
     of EVSYS_CHSTATUS_USRRDY_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_CHSTATUS_USRRDY
   type EVSYS_CHSTATUS_USRRDY_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  USRRDY as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  USRRDY as an array
            Arr : EVSYS_CHSTATUS_USRRDY_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_CHSTATUS_USRRDY_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_CHSTATUS_CHBUSY array element
   subtype EVSYS_CHSTATUS_CHBUSY_Element is ATSAMD21G18A.Bit;

   --  EVSYS_CHSTATUS_CHBUSY array
   type EVSYS_CHSTATUS_CHBUSY_Field_Array is array (0 .. 7)
     of EVSYS_CHSTATUS_CHBUSY_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_CHSTATUS_CHBUSY
   type EVSYS_CHSTATUS_CHBUSY_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  CHBUSY as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  CHBUSY as an array
            Arr : EVSYS_CHSTATUS_CHBUSY_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_CHSTATUS_CHBUSY_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_CHSTATUS_USRRDY array
   type EVSYS_CHSTATUS_USRRDY_Field_Array_1 is array (8 .. 11)
     of EVSYS_CHSTATUS_USRRDY_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_CHSTATUS_USRRDY
   type EVSYS_CHSTATUS_USRRDY_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  USRRDY as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  USRRDY as an array
            Arr : EVSYS_CHSTATUS_USRRDY_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_CHSTATUS_USRRDY_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  EVSYS_CHSTATUS_CHBUSY array
   type EVSYS_CHSTATUS_CHBUSY_Field_Array_1 is array (8 .. 11)
     of EVSYS_CHSTATUS_CHBUSY_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_CHSTATUS_CHBUSY
   type EVSYS_CHSTATUS_CHBUSY_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  CHBUSY as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  CHBUSY as an array
            Arr : EVSYS_CHSTATUS_CHBUSY_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_CHSTATUS_CHBUSY_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  Channel Status
   type EVSYS_CHSTATUS_Register is record
      --  Read-only. Channel 0 User Ready
      USRRDY         : EVSYS_CHSTATUS_USRRDY_Field;
      --  Read-only. Channel 0 Busy
      CHBUSY         : EVSYS_CHSTATUS_CHBUSY_Field;
      --  Read-only. Channel 8 User Ready
      USRRDY_1       : EVSYS_CHSTATUS_USRRDY_Field_1;
      --  unspecified
      Reserved_20_23 : ATSAMD21G18A.UInt4;
      --  Read-only. Channel 8 Busy
      CHBUSY_1       : EVSYS_CHSTATUS_CHBUSY_Field_1;
      --  unspecified
      Reserved_28_31 : ATSAMD21G18A.UInt4;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EVSYS_CHSTATUS_Register use record
      USRRDY         at 0 range 0 .. 7;
      CHBUSY         at 0 range 8 .. 15;
      USRRDY_1       at 0 range 16 .. 19;
      Reserved_20_23 at 0 range 20 .. 23;
      CHBUSY_1       at 0 range 24 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   --  EVSYS_INTENCLR_OVR array element
   subtype EVSYS_INTENCLR_OVR_Element is ATSAMD21G18A.Bit;

   --  EVSYS_INTENCLR_OVR array
   type EVSYS_INTENCLR_OVR_Field_Array is array (0 .. 7)
     of EVSYS_INTENCLR_OVR_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_INTENCLR_OVR
   type EVSYS_INTENCLR_OVR_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  OVR as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  OVR as an array
            Arr : EVSYS_INTENCLR_OVR_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_INTENCLR_OVR_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_INTENCLR_EVD array element
   subtype EVSYS_INTENCLR_EVD_Element is ATSAMD21G18A.Bit;

   --  EVSYS_INTENCLR_EVD array
   type EVSYS_INTENCLR_EVD_Field_Array is array (0 .. 7)
     of EVSYS_INTENCLR_EVD_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_INTENCLR_EVD
   type EVSYS_INTENCLR_EVD_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EVD as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  EVD as an array
            Arr : EVSYS_INTENCLR_EVD_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_INTENCLR_EVD_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_INTENCLR_OVR array
   type EVSYS_INTENCLR_OVR_Field_Array_1 is array (8 .. 11)
     of EVSYS_INTENCLR_OVR_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_INTENCLR_OVR
   type EVSYS_INTENCLR_OVR_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  OVR as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  OVR as an array
            Arr : EVSYS_INTENCLR_OVR_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_INTENCLR_OVR_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  EVSYS_INTENCLR_EVD array
   type EVSYS_INTENCLR_EVD_Field_Array_1 is array (8 .. 11)
     of EVSYS_INTENCLR_EVD_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_INTENCLR_EVD
   type EVSYS_INTENCLR_EVD_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EVD as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  EVD as an array
            Arr : EVSYS_INTENCLR_EVD_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_INTENCLR_EVD_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  Interrupt Enable Clear
   type EVSYS_INTENCLR_Register is record
      --  Channel 0 Overrun Interrupt Enable
      OVR            : EVSYS_INTENCLR_OVR_Field :=
                        (As_Array => False, Val => 16#0#);
      --  Channel 0 Event Detection Interrupt Enable
      EVD            : EVSYS_INTENCLR_EVD_Field :=
                        (As_Array => False, Val => 16#0#);
      --  Channel 8 Overrun Interrupt Enable
      OVR_1          : EVSYS_INTENCLR_OVR_Field_1 :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_20_23 : ATSAMD21G18A.UInt4 := 16#0#;
      --  Channel 8 Event Detection Interrupt Enable
      EVD_1          : EVSYS_INTENCLR_EVD_Field_1 :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_28_31 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EVSYS_INTENCLR_Register use record
      OVR            at 0 range 0 .. 7;
      EVD            at 0 range 8 .. 15;
      OVR_1          at 0 range 16 .. 19;
      Reserved_20_23 at 0 range 20 .. 23;
      EVD_1          at 0 range 24 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   --  EVSYS_INTENSET_OVR array element
   subtype EVSYS_INTENSET_OVR_Element is ATSAMD21G18A.Bit;

   --  EVSYS_INTENSET_OVR array
   type EVSYS_INTENSET_OVR_Field_Array is array (0 .. 7)
     of EVSYS_INTENSET_OVR_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_INTENSET_OVR
   type EVSYS_INTENSET_OVR_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  OVR as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  OVR as an array
            Arr : EVSYS_INTENSET_OVR_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_INTENSET_OVR_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_INTENSET_EVD array element
   subtype EVSYS_INTENSET_EVD_Element is ATSAMD21G18A.Bit;

   --  EVSYS_INTENSET_EVD array
   type EVSYS_INTENSET_EVD_Field_Array is array (0 .. 7)
     of EVSYS_INTENSET_EVD_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_INTENSET_EVD
   type EVSYS_INTENSET_EVD_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EVD as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  EVD as an array
            Arr : EVSYS_INTENSET_EVD_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_INTENSET_EVD_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_INTENSET_OVR array
   type EVSYS_INTENSET_OVR_Field_Array_1 is array (8 .. 11)
     of EVSYS_INTENSET_OVR_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_INTENSET_OVR
   type EVSYS_INTENSET_OVR_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  OVR as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  OVR as an array
            Arr : EVSYS_INTENSET_OVR_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_INTENSET_OVR_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  EVSYS_INTENSET_EVD array
   type EVSYS_INTENSET_EVD_Field_Array_1 is array (8 .. 11)
     of EVSYS_INTENSET_EVD_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_INTENSET_EVD
   type EVSYS_INTENSET_EVD_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EVD as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  EVD as an array
            Arr : EVSYS_INTENSET_EVD_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_INTENSET_EVD_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  Interrupt Enable Set
   type EVSYS_INTENSET_Register is record
      --  Channel 0 Overrun Interrupt Enable
      OVR            : EVSYS_INTENSET_OVR_Field :=
                        (As_Array => False, Val => 16#0#);
      --  Channel 0 Event Detection Interrupt Enable
      EVD            : EVSYS_INTENSET_EVD_Field :=
                        (As_Array => False, Val => 16#0#);
      --  Channel 8 Overrun Interrupt Enable
      OVR_1          : EVSYS_INTENSET_OVR_Field_1 :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_20_23 : ATSAMD21G18A.UInt4 := 16#0#;
      --  Channel 8 Event Detection Interrupt Enable
      EVD_1          : EVSYS_INTENSET_EVD_Field_1 :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_28_31 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EVSYS_INTENSET_Register use record
      OVR            at 0 range 0 .. 7;
      EVD            at 0 range 8 .. 15;
      OVR_1          at 0 range 16 .. 19;
      Reserved_20_23 at 0 range 20 .. 23;
      EVD_1          at 0 range 24 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   --  EVSYS_INTFLAG_OVR array element
   subtype EVSYS_INTFLAG_OVR_Element is ATSAMD21G18A.Bit;

   --  EVSYS_INTFLAG_OVR array
   type EVSYS_INTFLAG_OVR_Field_Array is array (0 .. 7)
     of EVSYS_INTFLAG_OVR_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_INTFLAG_OVR
   type EVSYS_INTFLAG_OVR_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  OVR as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  OVR as an array
            Arr : EVSYS_INTFLAG_OVR_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_INTFLAG_OVR_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_INTFLAG_EVD array element
   subtype EVSYS_INTFLAG_EVD_Element is ATSAMD21G18A.Bit;

   --  EVSYS_INTFLAG_EVD array
   type EVSYS_INTFLAG_EVD_Field_Array is array (0 .. 7)
     of EVSYS_INTFLAG_EVD_Element
     with Component_Size => 1, Size => 8;

   --  Type definition for EVSYS_INTFLAG_EVD
   type EVSYS_INTFLAG_EVD_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EVD as a value
            Val : ATSAMD21G18A.Byte;
         when True =>
            --  EVD as an array
            Arr : EVSYS_INTFLAG_EVD_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 8;

   for EVSYS_INTFLAG_EVD_Field use record
      Val at 0 range 0 .. 7;
      Arr at 0 range 0 .. 7;
   end record;

   --  EVSYS_INTFLAG_OVR array
   type EVSYS_INTFLAG_OVR_Field_Array_1 is array (8 .. 11)
     of EVSYS_INTFLAG_OVR_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_INTFLAG_OVR
   type EVSYS_INTFLAG_OVR_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  OVR as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  OVR as an array
            Arr : EVSYS_INTFLAG_OVR_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_INTFLAG_OVR_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  EVSYS_INTFLAG_EVD array
   type EVSYS_INTFLAG_EVD_Field_Array_1 is array (8 .. 11)
     of EVSYS_INTFLAG_EVD_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for EVSYS_INTFLAG_EVD
   type EVSYS_INTFLAG_EVD_Field_1
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EVD as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  EVD as an array
            Arr : EVSYS_INTFLAG_EVD_Field_Array_1;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for EVSYS_INTFLAG_EVD_Field_1 use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  Interrupt Flag Status and Clear
   type EVSYS_INTFLAG_Register is record
      --  Channel 0 Overrun
      OVR            : EVSYS_INTFLAG_OVR_Field :=
                        (As_Array => False, Val => 16#0#);
      --  Channel 0 Event Detection
      EVD            : EVSYS_INTFLAG_EVD_Field :=
                        (As_Array => False, Val => 16#0#);
      --  Channel 8 Overrun
      OVR_1          : EVSYS_INTFLAG_OVR_Field_1 :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_20_23 : ATSAMD21G18A.UInt4 := 16#0#;
      --  Channel 8 Event Detection
      EVD_1          : EVSYS_INTFLAG_EVD_Field_1 :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_28_31 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EVSYS_INTFLAG_Register use record
      OVR            at 0 range 0 .. 7;
      EVD            at 0 range 8 .. 15;
      OVR_1          at 0 range 16 .. 19;
      Reserved_20_23 at 0 range 20 .. 23;
      EVD_1          at 0 range 24 .. 27;
      Reserved_28_31 at 0 range 28 .. 31;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  Event System Interface
   type EVSYS_Peripheral is record
      --  Control
      CTRL     : aliased EVSYS_CTRL_Register;
      --  Channel
      CHANNEL  : aliased EVSYS_CHANNEL_Register;
      --  User Multiplexer
      USER     : aliased EVSYS_USER_Register;
      --  Channel Status
      CHSTATUS : aliased EVSYS_CHSTATUS_Register;
      --  Interrupt Enable Clear
      INTENCLR : aliased EVSYS_INTENCLR_Register;
      --  Interrupt Enable Set
      INTENSET : aliased EVSYS_INTENSET_Register;
      --  Interrupt Flag Status and Clear
      INTFLAG  : aliased EVSYS_INTFLAG_Register;
   end record
     with Volatile;

   for EVSYS_Peripheral use record
      CTRL     at 16#0# range 0 .. 7;
      CHANNEL  at 16#4# range 0 .. 31;
      USER     at 16#8# range 0 .. 15;
      CHSTATUS at 16#C# range 0 .. 31;
      INTENCLR at 16#10# range 0 .. 31;
      INTENSET at 16#14# range 0 .. 31;
      INTFLAG  at 16#18# range 0 .. 31;
   end record;

   --  Event System Interface
   EVSYS_Periph : aliased EVSYS_Peripheral
     with Import, Address => System'To_Address (16#42000400#);

end ATSAMD21G18A.EVSYS;
