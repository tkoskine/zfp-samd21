--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.HMATRIXB is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   --  Special Function

   --  Special Function
   type HMATRIXB_SFR_Registers is array (0 .. 15) of ATSAMD21G18A.UInt32
     with Volatile;

   -----------------
   -- Peripherals --
   -----------------

   --  HSB Matrix
   type HMATRIX_Peripheral is record
      --  Priority A for Slave
      PRAS0  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS0  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS1  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS1  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS2  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS2  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS3  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS3  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS4  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS4  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS5  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS5  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS6  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS6  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS7  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS7  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS8  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS8  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS9  : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS9  : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS10 : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS10 : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS11 : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS11 : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS12 : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS12 : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS13 : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS13 : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS14 : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS14 : aliased ATSAMD21G18A.UInt32;
      --  Priority A for Slave
      PRAS15 : aliased ATSAMD21G18A.UInt32;
      --  Priority B for Slave
      PRBS15 : aliased ATSAMD21G18A.UInt32;
      --  Special Function
      SFR    : aliased HMATRIXB_SFR_Registers;
   end record
     with Volatile;

   for HMATRIX_Peripheral use record
      PRAS0  at 16#80# range 0 .. 31;
      PRBS0  at 16#84# range 0 .. 31;
      PRAS1  at 16#88# range 0 .. 31;
      PRBS1  at 16#8C# range 0 .. 31;
      PRAS2  at 16#90# range 0 .. 31;
      PRBS2  at 16#94# range 0 .. 31;
      PRAS3  at 16#98# range 0 .. 31;
      PRBS3  at 16#9C# range 0 .. 31;
      PRAS4  at 16#A0# range 0 .. 31;
      PRBS4  at 16#A4# range 0 .. 31;
      PRAS5  at 16#A8# range 0 .. 31;
      PRBS5  at 16#AC# range 0 .. 31;
      PRAS6  at 16#B0# range 0 .. 31;
      PRBS6  at 16#B4# range 0 .. 31;
      PRAS7  at 16#B8# range 0 .. 31;
      PRBS7  at 16#BC# range 0 .. 31;
      PRAS8  at 16#C0# range 0 .. 31;
      PRBS8  at 16#C4# range 0 .. 31;
      PRAS9  at 16#C8# range 0 .. 31;
      PRBS9  at 16#CC# range 0 .. 31;
      PRAS10 at 16#D0# range 0 .. 31;
      PRBS10 at 16#D4# range 0 .. 31;
      PRAS11 at 16#D8# range 0 .. 31;
      PRBS11 at 16#DC# range 0 .. 31;
      PRAS12 at 16#E0# range 0 .. 31;
      PRBS12 at 16#E4# range 0 .. 31;
      PRAS13 at 16#E8# range 0 .. 31;
      PRBS13 at 16#EC# range 0 .. 31;
      PRAS14 at 16#F0# range 0 .. 31;
      PRBS14 at 16#F4# range 0 .. 31;
      PRAS15 at 16#F8# range 0 .. 31;
      PRBS15 at 16#FC# range 0 .. 31;
      SFR    at 16#110# range 0 .. 511;
   end record;

   --  HSB Matrix
   HMATRIX_Periph : aliased HMATRIX_Peripheral
     with Import, Address => System'To_Address (16#41007000#);

end ATSAMD21G18A.HMATRIXB;
