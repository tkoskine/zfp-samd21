--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with Interfaces;  use Interfaces;
with System;

--  Atmel ATSAMD21G18A device: Cortex-M0+ Microcontroller with 256KB Flash,
--  32KB SRAM, 48-pin package (refer to
--  http://www.atmel.com/devices/SAMD21G18A.aspx for more)
package ATSAMD21G18A is
   pragma Preelaborate;

   ---------------
   -- Base type --
   ---------------

   type UInt32 is new Interfaces.Unsigned_32;
   type UInt16 is new Interfaces.Unsigned_16;
   type Byte is new Interfaces.Unsigned_8;
   type Bit is mod 2**1
     with Size => 1;
   type UInt2 is mod 2**2
     with Size => 2;
   type UInt3 is mod 2**3
     with Size => 3;
   type UInt4 is mod 2**4
     with Size => 4;
   type UInt5 is mod 2**5
     with Size => 5;
   type UInt6 is mod 2**6
     with Size => 6;
   type UInt7 is mod 2**7
     with Size => 7;
   type UInt9 is mod 2**9
     with Size => 9;
   type UInt10 is mod 2**10
     with Size => 10;
   type UInt11 is mod 2**11
     with Size => 11;
   type UInt12 is mod 2**12
     with Size => 12;
   type UInt13 is mod 2**13
     with Size => 13;
   type UInt14 is mod 2**14
     with Size => 14;
   type UInt15 is mod 2**15
     with Size => 15;
   type UInt17 is mod 2**17
     with Size => 17;
   type UInt18 is mod 2**18
     with Size => 18;
   type UInt19 is mod 2**19
     with Size => 19;
   type UInt20 is mod 2**20
     with Size => 20;
   type UInt21 is mod 2**21
     with Size => 21;
   type UInt22 is mod 2**22
     with Size => 22;
   type UInt23 is mod 2**23
     with Size => 23;
   type UInt24 is mod 2**24
     with Size => 24;
   type UInt25 is mod 2**25
     with Size => 25;
   type UInt26 is mod 2**26
     with Size => 26;
   type UInt27 is mod 2**27
     with Size => 27;
   type UInt28 is mod 2**28
     with Size => 28;
   type UInt29 is mod 2**29
     with Size => 29;
   type UInt30 is mod 2**30
     with Size => 30;
   type UInt31 is mod 2**31
     with Size => 31;

   --------------------
   -- Base addresses --
   --------------------

   AC_Base : constant System.Address :=
     System'To_Address (16#42004400#);
   ADC_Base : constant System.Address :=
     System'To_Address (16#42004000#);
   DAC_Base : constant System.Address :=
     System'To_Address (16#42004800#);
   DMAC_Base : constant System.Address :=
     System'To_Address (16#41004800#);
   DSU_Base : constant System.Address :=
     System'To_Address (16#41002000#);
   EIC_Base : constant System.Address :=
     System'To_Address (16#40001800#);
   EVSYS_Base : constant System.Address :=
     System'To_Address (16#42000400#);
   GCLK_Base : constant System.Address :=
     System'To_Address (16#40000C00#);
   HMATRIX_Base : constant System.Address :=
     System'To_Address (16#41007000#);
   I2S_Base : constant System.Address :=
     System'To_Address (16#42005000#);
   MTB_Base : constant System.Address :=
     System'To_Address (16#41006000#);
   NVMCTRL_Base : constant System.Address :=
     System'To_Address (16#41004000#);
   PAC0_Base : constant System.Address :=
     System'To_Address (16#40000000#);
   PAC1_Base : constant System.Address :=
     System'To_Address (16#41000000#);
   PAC2_Base : constant System.Address :=
     System'To_Address (16#42000000#);
   PM_Base : constant System.Address :=
     System'To_Address (16#40000400#);
   PORT0_Base : constant System.Address :=
     System'To_Address (16#41004400#);
   PORT1_Base : constant System.Address :=
     System'To_Address (16#41004480#);
   RTC_Base : constant System.Address :=
     System'To_Address (16#40001400#);
   SERCOM0_Base : constant System.Address :=
     System'To_Address (16#42000800#);
   SERCOM1_Base : constant System.Address :=
     System'To_Address (16#42000C00#);
   SERCOM2_Base : constant System.Address :=
     System'To_Address (16#42001000#);
   SERCOM3_Base : constant System.Address :=
     System'To_Address (16#42001400#);
   SERCOM4_Base : constant System.Address :=
     System'To_Address (16#42001800#);
   SERCOM5_Base : constant System.Address :=
     System'To_Address (16#42001C00#);
   SYSCTRL_Base : constant System.Address :=
     System'To_Address (16#40000800#);
   TC3_Base : constant System.Address :=
     System'To_Address (16#42002C00#);
   TC4_Base : constant System.Address :=
     System'To_Address (16#42003000#);
   TC5_Base : constant System.Address :=
     System'To_Address (16#42003400#);
   TCC0_Base : constant System.Address :=
     System'To_Address (16#42002000#);
   TCC1_Base : constant System.Address :=
     System'To_Address (16#42002400#);
   TCC2_Base : constant System.Address :=
     System'To_Address (16#42002800#);
   USB_Base : constant System.Address :=
     System'To_Address (16#41005000#);
   WDT_Base : constant System.Address :=
     System'To_Address (16#40001000#);

end ATSAMD21G18A;
