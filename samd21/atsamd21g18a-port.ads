--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.PORT is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype PORT_WRCONFIG_PINMASK_Field is ATSAMD21G18A.UInt16;
   subtype PORT_WRCONFIG_PMUXEN_Field is ATSAMD21G18A.Bit;
   subtype PORT_WRCONFIG_INEN_Field is ATSAMD21G18A.Bit;
   subtype PORT_WRCONFIG_PULLEN_Field is ATSAMD21G18A.Bit;
   subtype PORT_WRCONFIG_DRVSTR_Field is ATSAMD21G18A.Bit;
   subtype PORT_WRCONFIG_PMUX_Field is ATSAMD21G18A.UInt4;
   subtype PORT_WRCONFIG_WRPMUX_Field is ATSAMD21G18A.Bit;
   subtype PORT_WRCONFIG_WRPINCFG_Field is ATSAMD21G18A.Bit;
   subtype PORT_WRCONFIG_HWSEL_Field is ATSAMD21G18A.Bit;

   --  Write Configuration
   type PORT_WRCONFIG_Register is record
      --  Write-only. Pin Mask for Multiple Pin Configuration
      PINMASK        : PORT_WRCONFIG_PINMASK_Field := 16#0#;
      --  Write-only. Peripheral Multiplexer Enable
      PMUXEN         : PORT_WRCONFIG_PMUXEN_Field := 16#0#;
      --  Write-only. Input Enable
      INEN           : PORT_WRCONFIG_INEN_Field := 16#0#;
      --  Write-only. Pull Enable
      PULLEN         : PORT_WRCONFIG_PULLEN_Field := 16#0#;
      --  unspecified
      Reserved_19_21 : ATSAMD21G18A.UInt3 := 16#0#;
      --  Write-only. Output Driver Strength Selection
      DRVSTR         : PORT_WRCONFIG_DRVSTR_Field := 16#0#;
      --  unspecified
      Reserved_23_23 : ATSAMD21G18A.Bit := 16#0#;
      --  Write-only. Peripheral Multiplexing
      PMUX           : PORT_WRCONFIG_PMUX_Field := 16#0#;
      --  Write-only. Write PMUX
      WRPMUX         : PORT_WRCONFIG_WRPMUX_Field := 16#0#;
      --  unspecified
      Reserved_29_29 : ATSAMD21G18A.Bit := 16#0#;
      --  Write-only. Write PINCFG
      WRPINCFG       : PORT_WRCONFIG_WRPINCFG_Field := 16#0#;
      --  Write-only. Half-Word Select
      HWSEL          : PORT_WRCONFIG_HWSEL_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for PORT_WRCONFIG_Register use record
      PINMASK        at 0 range 0 .. 15;
      PMUXEN         at 0 range 16 .. 16;
      INEN           at 0 range 17 .. 17;
      PULLEN         at 0 range 18 .. 18;
      Reserved_19_21 at 0 range 19 .. 21;
      DRVSTR         at 0 range 22 .. 22;
      Reserved_23_23 at 0 range 23 .. 23;
      PMUX           at 0 range 24 .. 27;
      WRPMUX         at 0 range 28 .. 28;
      Reserved_29_29 at 0 range 29 .. 29;
      WRPINCFG       at 0 range 30 .. 30;
      HWSEL          at 0 range 31 .. 31;
   end record;

   --  Peripheral Multiplexing Even
   type PMUX_PMUXESelect is
     (
      --  Peripheral function A selected
      A,
      --  Peripheral function B selected
      B,
      --  Peripheral function C selected
      C,
      --  Peripheral function D selected
      D,
      --  Peripheral function E selected
      E,
      --  Peripheral function F selected
      F,
      --  Peripheral function G selected
      G,
      --  Peripheral function H selected
      H)
     with Size => 4;
   for PMUX_PMUXESelect use
     (A => 0,
      B => 1,
      C => 2,
      D => 3,
      E => 4,
      F => 5,
      G => 6,
      H => 7);

   --  Peripheral Multiplexing Odd
   type PMUX_PMUXOSelect is
     (
      --  Peripheral function A selected
      A,
      --  Peripheral function B selected
      B,
      --  Peripheral function C selected
      C,
      --  Peripheral function D selected
      D,
      --  Peripheral function E selected
      E,
      --  Peripheral function F selected
      F,
      --  Peripheral function G selected
      G,
      --  Peripheral function H selected
      H)
     with Size => 4;
   for PMUX_PMUXOSelect use
     (A => 0,
      B => 1,
      C => 2,
      D => 3,
      E => 4,
      F => 5,
      G => 6,
      H => 7);

   --  Peripheral Multiplexing n - Group 0
   type PORT_PMUX_Register is record
      --  Peripheral Multiplexing Even
      PMUXE : PMUX_PMUXESelect := ATSAMD21G18A.PORT.A;
      --  Peripheral Multiplexing Odd
      PMUXO : PMUX_PMUXOSelect := ATSAMD21G18A.PORT.A;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for PORT_PMUX_Register use record
      PMUXE at 0 range 0 .. 3;
      PMUXO at 0 range 4 .. 7;
   end record;

   --  Peripheral Multiplexing n - Group 0
   type PORT_PMUX_Registers is array (0 .. 15) of PORT_PMUX_Register
     with Volatile;

   subtype PORT_PINCFG_PMUXEN_Field is ATSAMD21G18A.Bit;
   subtype PORT_PINCFG_INEN_Field is ATSAMD21G18A.Bit;
   subtype PORT_PINCFG_PULLEN_Field is ATSAMD21G18A.Bit;
   subtype PORT_PINCFG_DRVSTR_Field is ATSAMD21G18A.Bit;

   --  Pin Configuration n - Group 0
   type PORT_PINCFG_Register is record
      --  Peripheral Multiplexer Enable
      PMUXEN       : PORT_PINCFG_PMUXEN_Field := 16#0#;
      --  Input Enable
      INEN         : PORT_PINCFG_INEN_Field := 16#0#;
      --  Pull Enable
      PULLEN       : PORT_PINCFG_PULLEN_Field := 16#0#;
      --  unspecified
      Reserved_3_5 : ATSAMD21G18A.UInt3 := 16#0#;
      --  Write-only. Output Driver Strength Selection
      DRVSTR       : PORT_PINCFG_DRVSTR_Field := 16#0#;
      --  unspecified
      Reserved_7_7 : ATSAMD21G18A.Bit := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for PORT_PINCFG_Register use record
      PMUXEN       at 0 range 0 .. 0;
      INEN         at 0 range 1 .. 1;
      PULLEN       at 0 range 2 .. 2;
      Reserved_3_5 at 0 range 3 .. 5;
      DRVSTR       at 0 range 6 .. 6;
      Reserved_7_7 at 0 range 7 .. 7;
   end record;

   --  Pin Configuration n - Group 0
   type PORT_PINCFG_Registers is array (0 .. 31) of PORT_PINCFG_Register
     with Volatile;

   -----------------
   -- Peripherals --
   -----------------

   --  Port Module
   type PORT_Peripheral is record
      --  Data Direction
      DIR      : aliased ATSAMD21G18A.UInt32;
      --  Data Direction Clear
      DIRCLR   : aliased ATSAMD21G18A.UInt32;
      --  Data Direction Set
      DIRSET   : aliased ATSAMD21G18A.UInt32;
      --  Data Direction Toggle
      DIRTGL   : aliased ATSAMD21G18A.UInt32;
      --  Data Output Value
      OUT_k    : aliased ATSAMD21G18A.UInt32;
      --  Data Output Value Clear
      OUTCLR   : aliased ATSAMD21G18A.UInt32;
      --  Data Output Value Set
      OUTSET   : aliased ATSAMD21G18A.UInt32;
      --  Data Output Value Toggle
      OUTTGL   : aliased ATSAMD21G18A.UInt32;
      --  Data Input Value
      IN_k     : aliased ATSAMD21G18A.UInt32;
      --  Control
      CTRL     : aliased ATSAMD21G18A.UInt32;
      --  Write Configuration
      WRCONFIG : aliased PORT_WRCONFIG_Register;
      --  Peripheral Multiplexing n - Group 0
      PMUX     : aliased PORT_PMUX_Registers;
      --  Pin Configuration n - Group 0
      PINCFG   : aliased PORT_PINCFG_Registers;
   end record
     with Volatile;

   for PORT_Peripheral use record
      DIR      at 16#0# range 0 .. 31;
      DIRCLR   at 16#4# range 0 .. 31;
      DIRSET   at 16#8# range 0 .. 31;
      DIRTGL   at 16#C# range 0 .. 31;
      OUT_k    at 16#10# range 0 .. 31;
      OUTCLR   at 16#14# range 0 .. 31;
      OUTSET   at 16#18# range 0 .. 31;
      OUTTGL   at 16#1C# range 0 .. 31;
      IN_k     at 16#20# range 0 .. 31;
      CTRL     at 16#24# range 0 .. 31;
      WRCONFIG at 16#28# range 0 .. 31;
      PMUX     at 16#30# range 0 .. 127;
      PINCFG   at 16#40# range 0 .. 255;
   end record;

   --  Port Module
   PORT0_Periph : aliased PORT_Peripheral
     with Import, Address => System'To_Address (16#41004400#);

   --  Port Module
   PORT1_Periph : aliased PORT_Peripheral
     with Import, Address => System'To_Address (16#41004480#);

end ATSAMD21G18A.PORT;
