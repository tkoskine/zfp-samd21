--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.PAC is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype PAC_WPCLR_WP_Field is ATSAMD21G18A.UInt31;

   --  Write Protection Clear
   type PAC_WPCLR_Register is record
      --  unspecified
      Reserved_0_0 : ATSAMD21G18A.Bit := 16#0#;
      --  Write Protection Clear
      WP           : PAC_WPCLR_WP_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for PAC_WPCLR_Register use record
      Reserved_0_0 at 0 range 0 .. 0;
      WP           at 0 range 1 .. 31;
   end record;

   subtype PAC_WPSET_WP_Field is ATSAMD21G18A.UInt31;

   --  Write Protection Set
   type PAC_WPSET_Register is record
      --  unspecified
      Reserved_0_0 : ATSAMD21G18A.Bit := 16#0#;
      --  Write Protection Set
      WP           : PAC_WPSET_WP_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for PAC_WPSET_Register use record
      Reserved_0_0 at 0 range 0 .. 0;
      WP           at 0 range 1 .. 31;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  Peripheral Access Controller 0
   type PAC_Peripheral is record
      --  Write Protection Clear
      WPCLR : aliased PAC_WPCLR_Register;
      --  Write Protection Set
      WPSET : aliased PAC_WPSET_Register;
   end record
     with Volatile;

   for PAC_Peripheral use record
      WPCLR at 16#0# range 0 .. 31;
      WPSET at 16#4# range 0 .. 31;
   end record;

   --  Peripheral Access Controller 0
   PAC0_Periph : aliased PAC_Peripheral
     with Import, Address => System'To_Address (16#40000000#);

   --  Peripheral Access Controller 1
   PAC1_Periph : aliased PAC_Peripheral
     with Import, Address => System'To_Address (16#41000000#);

   --  Peripheral Access Controller 2
   PAC2_Periph : aliased PAC_Peripheral
     with Import, Address => System'To_Address (16#42000000#);

end ATSAMD21G18A.PAC;
