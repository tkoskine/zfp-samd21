--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.EIC is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype EIC_CTRL_SWRST_Field is ATSAMD21G18A.Bit;
   subtype EIC_CTRL_ENABLE_Field is ATSAMD21G18A.Bit;

   --  Control
   type EIC_CTRL_Register is record
      --  Software Reset
      SWRST        : EIC_CTRL_SWRST_Field := 16#0#;
      --  Enable
      ENABLE       : EIC_CTRL_ENABLE_Field := 16#0#;
      --  unspecified
      Reserved_2_7 : ATSAMD21G18A.UInt6 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for EIC_CTRL_Register use record
      SWRST        at 0 range 0 .. 0;
      ENABLE       at 0 range 1 .. 1;
      Reserved_2_7 at 0 range 2 .. 7;
   end record;

   subtype EIC_STATUS_SYNCBUSY_Field is ATSAMD21G18A.Bit;

   --  Status
   type EIC_STATUS_Register is record
      --  unspecified
      Reserved_0_6 : ATSAMD21G18A.UInt7;
      --  Read-only. Synchronization Busy
      SYNCBUSY     : EIC_STATUS_SYNCBUSY_Field;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for EIC_STATUS_Register use record
      Reserved_0_6 at 0 range 0 .. 6;
      SYNCBUSY     at 0 range 7 .. 7;
   end record;

   --  Non-Maskable Interrupt Sense
   type NMICTRL_NMISENSESelect is
     (
      --  No detection
      None,
      --  Rising-edge detection
      Rise,
      --  Falling-edge detection
      Fall,
      --  Both-edges detection
      Both,
      --  High-level detection
      High,
      --  Low-level detection
      Low)
     with Size => 3;
   for NMICTRL_NMISENSESelect use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_NMICTRL_NMIFILTEN_Field is ATSAMD21G18A.Bit;

   --  Non-Maskable Interrupt Control
   type EIC_NMICTRL_Register is record
      --  Non-Maskable Interrupt Sense
      NMISENSE     : NMICTRL_NMISENSESelect := ATSAMD21G18A.EIC.None;
      --  Non-Maskable Interrupt Filter Enable
      NMIFILTEN    : EIC_NMICTRL_NMIFILTEN_Field := 16#0#;
      --  unspecified
      Reserved_4_7 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for EIC_NMICTRL_Register use record
      NMISENSE     at 0 range 0 .. 2;
      NMIFILTEN    at 0 range 3 .. 3;
      Reserved_4_7 at 0 range 4 .. 7;
   end record;

   subtype EIC_NMIFLAG_NMI_Field is ATSAMD21G18A.Bit;

   --  Non-Maskable Interrupt Flag Status and Clear
   type EIC_NMIFLAG_Register is record
      --  Non-Maskable Interrupt
      NMI          : EIC_NMIFLAG_NMI_Field := 16#0#;
      --  unspecified
      Reserved_1_7 : ATSAMD21G18A.UInt7 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for EIC_NMIFLAG_Register use record
      NMI          at 0 range 0 .. 0;
      Reserved_1_7 at 0 range 1 .. 7;
   end record;

   --  EIC_EVCTRL_EXTINTEO array element
   subtype EIC_EVCTRL_EXTINTEO_Element is ATSAMD21G18A.Bit;

   --  EIC_EVCTRL_EXTINTEO array
   type EIC_EVCTRL_EXTINTEO_Field_Array is array (0 .. 15)
     of EIC_EVCTRL_EXTINTEO_Element
     with Component_Size => 1, Size => 16;

   --  Type definition for EIC_EVCTRL_EXTINTEO
   type EIC_EVCTRL_EXTINTEO_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EXTINTEO as a value
            Val : ATSAMD21G18A.UInt16;
         when True =>
            --  EXTINTEO as an array
            Arr : EIC_EVCTRL_EXTINTEO_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 16;

   for EIC_EVCTRL_EXTINTEO_Field use record
      Val at 0 range 0 .. 15;
      Arr at 0 range 0 .. 15;
   end record;

   --  Event Control
   type EIC_EVCTRL_Register is record
      --  External Interrupt 0 Event Output Enable
      EXTINTEO       : EIC_EVCTRL_EXTINTEO_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_16_31 : ATSAMD21G18A.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EIC_EVCTRL_Register use record
      EXTINTEO       at 0 range 0 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   --  EIC_INTENCLR_EXTINT array element
   subtype EIC_INTENCLR_EXTINT_Element is ATSAMD21G18A.Bit;

   --  EIC_INTENCLR_EXTINT array
   type EIC_INTENCLR_EXTINT_Field_Array is array (0 .. 15)
     of EIC_INTENCLR_EXTINT_Element
     with Component_Size => 1, Size => 16;

   --  Type definition for EIC_INTENCLR_EXTINT
   type EIC_INTENCLR_EXTINT_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EXTINT as a value
            Val : ATSAMD21G18A.UInt16;
         when True =>
            --  EXTINT as an array
            Arr : EIC_INTENCLR_EXTINT_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 16;

   for EIC_INTENCLR_EXTINT_Field use record
      Val at 0 range 0 .. 15;
      Arr at 0 range 0 .. 15;
   end record;

   --  Interrupt Enable Clear
   type EIC_INTENCLR_Register is record
      --  External Interrupt 0 Enable
      EXTINT         : EIC_INTENCLR_EXTINT_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_16_31 : ATSAMD21G18A.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EIC_INTENCLR_Register use record
      EXTINT         at 0 range 0 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   --  EIC_INTENSET_EXTINT array element
   subtype EIC_INTENSET_EXTINT_Element is ATSAMD21G18A.Bit;

   --  EIC_INTENSET_EXTINT array
   type EIC_INTENSET_EXTINT_Field_Array is array (0 .. 15)
     of EIC_INTENSET_EXTINT_Element
     with Component_Size => 1, Size => 16;

   --  Type definition for EIC_INTENSET_EXTINT
   type EIC_INTENSET_EXTINT_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EXTINT as a value
            Val : ATSAMD21G18A.UInt16;
         when True =>
            --  EXTINT as an array
            Arr : EIC_INTENSET_EXTINT_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 16;

   for EIC_INTENSET_EXTINT_Field use record
      Val at 0 range 0 .. 15;
      Arr at 0 range 0 .. 15;
   end record;

   --  Interrupt Enable Set
   type EIC_INTENSET_Register is record
      --  External Interrupt 0 Enable
      EXTINT         : EIC_INTENSET_EXTINT_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_16_31 : ATSAMD21G18A.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EIC_INTENSET_Register use record
      EXTINT         at 0 range 0 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   --  EIC_INTFLAG_EXTINT array element
   subtype EIC_INTFLAG_EXTINT_Element is ATSAMD21G18A.Bit;

   --  EIC_INTFLAG_EXTINT array
   type EIC_INTFLAG_EXTINT_Field_Array is array (0 .. 15)
     of EIC_INTFLAG_EXTINT_Element
     with Component_Size => 1, Size => 16;

   --  Type definition for EIC_INTFLAG_EXTINT
   type EIC_INTFLAG_EXTINT_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  EXTINT as a value
            Val : ATSAMD21G18A.UInt16;
         when True =>
            --  EXTINT as an array
            Arr : EIC_INTFLAG_EXTINT_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 16;

   for EIC_INTFLAG_EXTINT_Field use record
      Val at 0 range 0 .. 15;
      Arr at 0 range 0 .. 15;
   end record;

   --  Interrupt Flag Status and Clear
   type EIC_INTFLAG_Register is record
      --  External Interrupt 0
      EXTINT         : EIC_INTFLAG_EXTINT_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_16_31 : ATSAMD21G18A.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EIC_INTFLAG_Register use record
      EXTINT         at 0 range 0 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   --  EIC_WAKEUP_WAKEUPEN array element
   subtype EIC_WAKEUP_WAKEUPEN_Element is ATSAMD21G18A.Bit;

   --  EIC_WAKEUP_WAKEUPEN array
   type EIC_WAKEUP_WAKEUPEN_Field_Array is array (0 .. 15)
     of EIC_WAKEUP_WAKEUPEN_Element
     with Component_Size => 1, Size => 16;

   --  Type definition for EIC_WAKEUP_WAKEUPEN
   type EIC_WAKEUP_WAKEUPEN_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  WAKEUPEN as a value
            Val : ATSAMD21G18A.UInt16;
         when True =>
            --  WAKEUPEN as an array
            Arr : EIC_WAKEUP_WAKEUPEN_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 16;

   for EIC_WAKEUP_WAKEUPEN_Field use record
      Val at 0 range 0 .. 15;
      Arr at 0 range 0 .. 15;
   end record;

   --  Wake-Up Enable
   type EIC_WAKEUP_Register is record
      --  External Interrupt 0 Wake-up Enable
      WAKEUPEN       : EIC_WAKEUP_WAKEUPEN_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_16_31 : ATSAMD21G18A.UInt16 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EIC_WAKEUP_Register use record
      WAKEUPEN       at 0 range 0 .. 15;
      Reserved_16_31 at 0 range 16 .. 31;
   end record;

   --  Input Sense 0 Configuration
   type CONFIG_SENSE0Select is
     (
      --  No detection
      None,
      --  Rising-edge detection
      Rise,
      --  Falling-edge detection
      Fall,
      --  Both-edges detection
      Both,
      --  High-level detection
      High,
      --  Low-level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE0Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN0_Field is ATSAMD21G18A.Bit;

   --  Input Sense 1 Configuration
   type CONFIG_SENSE1Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE1Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN1_Field is ATSAMD21G18A.Bit;

   --  Input Sense 2 Configuration
   type CONFIG_SENSE2Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE2Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN2_Field is ATSAMD21G18A.Bit;

   --  Input Sense 3 Configuration
   type CONFIG_SENSE3Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE3Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN3_Field is ATSAMD21G18A.Bit;

   --  Input Sense 4 Configuration
   type CONFIG_SENSE4Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE4Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN4_Field is ATSAMD21G18A.Bit;

   --  Input Sense 5 Configuration
   type CONFIG_SENSE5Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE5Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN5_Field is ATSAMD21G18A.Bit;

   --  Input Sense 6 Configuration
   type CONFIG_SENSE6Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE6Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN6_Field is ATSAMD21G18A.Bit;

   --  Input Sense 7 Configuration
   type CONFIG_SENSE7Select is
     (
      --  No detection
      None,
      --  Rising edge detection
      Rise,
      --  Falling edge detection
      Fall,
      --  Both edges detection
      Both,
      --  High level detection
      High,
      --  Low level detection
      Low)
     with Size => 3;
   for CONFIG_SENSE7Select use
     (None => 0,
      Rise => 1,
      Fall => 2,
      Both => 3,
      High => 4,
      Low => 5);

   subtype EIC_CONFIG_FILTEN7_Field is ATSAMD21G18A.Bit;

   --  Configuration n
   type EIC_CONFIG_Register is record
      --  Input Sense 0 Configuration
      SENSE0  : CONFIG_SENSE0Select := ATSAMD21G18A.EIC.None;
      --  Filter 0 Enable
      FILTEN0 : EIC_CONFIG_FILTEN0_Field := 16#0#;
      --  Input Sense 1 Configuration
      SENSE1  : CONFIG_SENSE1Select := ATSAMD21G18A.EIC.None;
      --  Filter 1 Enable
      FILTEN1 : EIC_CONFIG_FILTEN1_Field := 16#0#;
      --  Input Sense 2 Configuration
      SENSE2  : CONFIG_SENSE2Select := ATSAMD21G18A.EIC.None;
      --  Filter 2 Enable
      FILTEN2 : EIC_CONFIG_FILTEN2_Field := 16#0#;
      --  Input Sense 3 Configuration
      SENSE3  : CONFIG_SENSE3Select := ATSAMD21G18A.EIC.None;
      --  Filter 3 Enable
      FILTEN3 : EIC_CONFIG_FILTEN3_Field := 16#0#;
      --  Input Sense 4 Configuration
      SENSE4  : CONFIG_SENSE4Select := ATSAMD21G18A.EIC.None;
      --  Filter 4 Enable
      FILTEN4 : EIC_CONFIG_FILTEN4_Field := 16#0#;
      --  Input Sense 5 Configuration
      SENSE5  : CONFIG_SENSE5Select := ATSAMD21G18A.EIC.None;
      --  Filter 5 Enable
      FILTEN5 : EIC_CONFIG_FILTEN5_Field := 16#0#;
      --  Input Sense 6 Configuration
      SENSE6  : CONFIG_SENSE6Select := ATSAMD21G18A.EIC.None;
      --  Filter 6 Enable
      FILTEN6 : EIC_CONFIG_FILTEN6_Field := 16#0#;
      --  Input Sense 7 Configuration
      SENSE7  : CONFIG_SENSE7Select := ATSAMD21G18A.EIC.None;
      --  Filter 7 Enable
      FILTEN7 : EIC_CONFIG_FILTEN7_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for EIC_CONFIG_Register use record
      SENSE0  at 0 range 0 .. 2;
      FILTEN0 at 0 range 3 .. 3;
      SENSE1  at 0 range 4 .. 6;
      FILTEN1 at 0 range 7 .. 7;
      SENSE2  at 0 range 8 .. 10;
      FILTEN2 at 0 range 11 .. 11;
      SENSE3  at 0 range 12 .. 14;
      FILTEN3 at 0 range 15 .. 15;
      SENSE4  at 0 range 16 .. 18;
      FILTEN4 at 0 range 19 .. 19;
      SENSE5  at 0 range 20 .. 22;
      FILTEN5 at 0 range 23 .. 23;
      SENSE6  at 0 range 24 .. 26;
      FILTEN6 at 0 range 27 .. 27;
      SENSE7  at 0 range 28 .. 30;
      FILTEN7 at 0 range 31 .. 31;
   end record;

   --  Configuration n
   type EIC_CONFIG_Registers is array (0 .. 1) of EIC_CONFIG_Register
     with Volatile;

   -----------------
   -- Peripherals --
   -----------------

   --  External Interrupt Controller
   type EIC_Peripheral is record
      --  Control
      CTRL     : aliased EIC_CTRL_Register;
      --  Status
      STATUS   : aliased EIC_STATUS_Register;
      --  Non-Maskable Interrupt Control
      NMICTRL  : aliased EIC_NMICTRL_Register;
      --  Non-Maskable Interrupt Flag Status and Clear
      NMIFLAG  : aliased EIC_NMIFLAG_Register;
      --  Event Control
      EVCTRL   : aliased EIC_EVCTRL_Register;
      --  Interrupt Enable Clear
      INTENCLR : aliased EIC_INTENCLR_Register;
      --  Interrupt Enable Set
      INTENSET : aliased EIC_INTENSET_Register;
      --  Interrupt Flag Status and Clear
      INTFLAG  : aliased EIC_INTFLAG_Register;
      --  Wake-Up Enable
      WAKEUP   : aliased EIC_WAKEUP_Register;
      --  Configuration n
      CONFIG   : aliased EIC_CONFIG_Registers;
   end record
     with Volatile;

   for EIC_Peripheral use record
      CTRL     at 16#0# range 0 .. 7;
      STATUS   at 16#1# range 0 .. 7;
      NMICTRL  at 16#2# range 0 .. 7;
      NMIFLAG  at 16#3# range 0 .. 7;
      EVCTRL   at 16#4# range 0 .. 31;
      INTENCLR at 16#8# range 0 .. 31;
      INTENSET at 16#C# range 0 .. 31;
      INTFLAG  at 16#10# range 0 .. 31;
      WAKEUP   at 16#14# range 0 .. 31;
      CONFIG   at 16#18# range 0 .. 63;
   end record;

   --  External Interrupt Controller
   EIC_Periph : aliased EIC_Peripheral
     with Import, Address => System'To_Address (16#40001800#);

end ATSAMD21G18A.EIC;
