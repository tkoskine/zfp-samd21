--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.DAC is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype DAC_CTRLA_SWRST_Field is ATSAMD21G18A.Bit;
   subtype DAC_CTRLA_ENABLE_Field is ATSAMD21G18A.Bit;
   subtype DAC_CTRLA_RUNSTDBY_Field is ATSAMD21G18A.Bit;

   --  Control A
   type DAC_CTRLA_Register is record
      --  Software Reset
      SWRST        : DAC_CTRLA_SWRST_Field := 16#0#;
      --  Enable
      ENABLE       : DAC_CTRLA_ENABLE_Field := 16#0#;
      --  Run in Standby
      RUNSTDBY     : DAC_CTRLA_RUNSTDBY_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_CTRLA_Register use record
      SWRST        at 0 range 0 .. 0;
      ENABLE       at 0 range 1 .. 1;
      RUNSTDBY     at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DAC_CTRLB_EOEN_Field is ATSAMD21G18A.Bit;
   subtype DAC_CTRLB_IOEN_Field is ATSAMD21G18A.Bit;
   subtype DAC_CTRLB_LEFTADJ_Field is ATSAMD21G18A.Bit;
   subtype DAC_CTRLB_VPD_Field is ATSAMD21G18A.Bit;
   subtype DAC_CTRLB_BDWP_Field is ATSAMD21G18A.Bit;

   --  Reference Selection
   type CTRLB_REFSELSelect is
     (
      --  Internal 1.0V reference
      Int1V,
      --  AVCC
      Avcc,
      --  External reference
      Vrefp)
     with Size => 2;
   for CTRLB_REFSELSelect use
     (Int1V => 0,
      Avcc => 1,
      Vrefp => 2);

   --  Control B
   type DAC_CTRLB_Register is record
      --  External Output Enable
      EOEN         : DAC_CTRLB_EOEN_Field := 16#0#;
      --  Internal Output Enable
      IOEN         : DAC_CTRLB_IOEN_Field := 16#0#;
      --  Left Adjusted Data
      LEFTADJ      : DAC_CTRLB_LEFTADJ_Field := 16#0#;
      --  Voltage Pump Disable
      VPD          : DAC_CTRLB_VPD_Field := 16#0#;
      --  Bypass DATABUF Write Protection
      BDWP         : DAC_CTRLB_BDWP_Field := 16#0#;
      --  unspecified
      Reserved_5_5 : ATSAMD21G18A.Bit := 16#0#;
      --  Reference Selection
      REFSEL       : CTRLB_REFSELSelect := ATSAMD21G18A.DAC.Int1V;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_CTRLB_Register use record
      EOEN         at 0 range 0 .. 0;
      IOEN         at 0 range 1 .. 1;
      LEFTADJ      at 0 range 2 .. 2;
      VPD          at 0 range 3 .. 3;
      BDWP         at 0 range 4 .. 4;
      Reserved_5_5 at 0 range 5 .. 5;
      REFSEL       at 0 range 6 .. 7;
   end record;

   subtype DAC_EVCTRL_STARTEI_Field is ATSAMD21G18A.Bit;
   subtype DAC_EVCTRL_EMPTYEO_Field is ATSAMD21G18A.Bit;

   --  Event Control
   type DAC_EVCTRL_Register is record
      --  Start Conversion Event Input
      STARTEI      : DAC_EVCTRL_STARTEI_Field := 16#0#;
      --  Data Buffer Empty Event Output
      EMPTYEO      : DAC_EVCTRL_EMPTYEO_Field := 16#0#;
      --  unspecified
      Reserved_2_7 : ATSAMD21G18A.UInt6 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_EVCTRL_Register use record
      STARTEI      at 0 range 0 .. 0;
      EMPTYEO      at 0 range 1 .. 1;
      Reserved_2_7 at 0 range 2 .. 7;
   end record;

   subtype DAC_INTENCLR_UNDERRUN_Field is ATSAMD21G18A.Bit;
   subtype DAC_INTENCLR_EMPTY_Field is ATSAMD21G18A.Bit;
   subtype DAC_INTENCLR_SYNCRDY_Field is ATSAMD21G18A.Bit;

   --  Interrupt Enable Clear
   type DAC_INTENCLR_Register is record
      --  Underrun Interrupt Enable
      UNDERRUN     : DAC_INTENCLR_UNDERRUN_Field := 16#0#;
      --  Data Buffer Empty Interrupt Enable
      EMPTY        : DAC_INTENCLR_EMPTY_Field := 16#0#;
      --  Synchronization Ready Interrupt Enable
      SYNCRDY      : DAC_INTENCLR_SYNCRDY_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_INTENCLR_Register use record
      UNDERRUN     at 0 range 0 .. 0;
      EMPTY        at 0 range 1 .. 1;
      SYNCRDY      at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DAC_INTENSET_UNDERRUN_Field is ATSAMD21G18A.Bit;
   subtype DAC_INTENSET_EMPTY_Field is ATSAMD21G18A.Bit;
   subtype DAC_INTENSET_SYNCRDY_Field is ATSAMD21G18A.Bit;

   --  Interrupt Enable Set
   type DAC_INTENSET_Register is record
      --  Underrun Interrupt Enable
      UNDERRUN     : DAC_INTENSET_UNDERRUN_Field := 16#0#;
      --  Data Buffer Empty Interrupt Enable
      EMPTY        : DAC_INTENSET_EMPTY_Field := 16#0#;
      --  Synchronization Ready Interrupt Enable
      SYNCRDY      : DAC_INTENSET_SYNCRDY_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_INTENSET_Register use record
      UNDERRUN     at 0 range 0 .. 0;
      EMPTY        at 0 range 1 .. 1;
      SYNCRDY      at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DAC_INTFLAG_UNDERRUN_Field is ATSAMD21G18A.Bit;
   subtype DAC_INTFLAG_EMPTY_Field is ATSAMD21G18A.Bit;
   subtype DAC_INTFLAG_SYNCRDY_Field is ATSAMD21G18A.Bit;

   --  Interrupt Flag Status and Clear
   type DAC_INTFLAG_Register is record
      --  Underrun
      UNDERRUN     : DAC_INTFLAG_UNDERRUN_Field := 16#0#;
      --  Data Buffer Empty
      EMPTY        : DAC_INTFLAG_EMPTY_Field := 16#0#;
      --  Synchronization Ready
      SYNCRDY      : DAC_INTFLAG_SYNCRDY_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_INTFLAG_Register use record
      UNDERRUN     at 0 range 0 .. 0;
      EMPTY        at 0 range 1 .. 1;
      SYNCRDY      at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DAC_STATUS_SYNCBUSY_Field is ATSAMD21G18A.Bit;

   --  Status
   type DAC_STATUS_Register is record
      --  unspecified
      Reserved_0_6 : ATSAMD21G18A.UInt7;
      --  Read-only. Synchronization Busy Status
      SYNCBUSY     : DAC_STATUS_SYNCBUSY_Field;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DAC_STATUS_Register use record
      Reserved_0_6 at 0 range 0 .. 6;
      SYNCBUSY     at 0 range 7 .. 7;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  Digital Analog Converter
   type DAC_Peripheral is record
      --  Control A
      CTRLA    : aliased DAC_CTRLA_Register;
      --  Control B
      CTRLB    : aliased DAC_CTRLB_Register;
      --  Event Control
      EVCTRL   : aliased DAC_EVCTRL_Register;
      --  Interrupt Enable Clear
      INTENCLR : aliased DAC_INTENCLR_Register;
      --  Interrupt Enable Set
      INTENSET : aliased DAC_INTENSET_Register;
      --  Interrupt Flag Status and Clear
      INTFLAG  : aliased DAC_INTFLAG_Register;
      --  Status
      STATUS   : aliased DAC_STATUS_Register;
      --  Data
      DATA     : aliased ATSAMD21G18A.UInt16;
      --  Data Buffer
      DATABUF  : aliased ATSAMD21G18A.UInt16;
   end record
     with Volatile;

   for DAC_Peripheral use record
      CTRLA    at 16#0# range 0 .. 7;
      CTRLB    at 16#1# range 0 .. 7;
      EVCTRL   at 16#2# range 0 .. 7;
      INTENCLR at 16#4# range 0 .. 7;
      INTENSET at 16#5# range 0 .. 7;
      INTFLAG  at 16#6# range 0 .. 7;
      STATUS   at 16#7# range 0 .. 7;
      DATA     at 16#8# range 0 .. 15;
      DATABUF  at 16#C# range 0 .. 15;
   end record;

   --  Digital Analog Converter
   DAC_Periph : aliased DAC_Peripheral
     with Import, Address => System'To_Address (16#42004800#);

end ATSAMD21G18A.DAC;
