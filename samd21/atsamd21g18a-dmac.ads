--  ============================================================================
--  Atmel Microcontroller Software Support
--  ============================================================================
--  Copyright (c) 2015, Atmel Corporation
--
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following condition is met:
--
--  - Redistributions of source code must retain the above copyright notice,
--  this list of conditions and the disclaimer below.
--
--  Atmel's name may not be used to endorse or promote products derived from
--  this software without specific prior written permission.
--
--  DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
--  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
--  DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
--  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  ============================================================================  

--  This spec has been automatically generated from ATSAMD21G18A.svd

pragma Restrictions (No_Elaboration_Code);
pragma Ada_2012;
pragma Style_Checks (Off);

with System;

package ATSAMD21G18A.DMAC is
   pragma Preelaborate;

   ---------------
   -- Registers --
   ---------------

   subtype DMAC_CTRL_SWRST_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CTRL_DMAENABLE_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CTRL_CRCENABLE_Field is ATSAMD21G18A.Bit;
   --  DMAC_CTRL_LVLEN array element
   subtype DMAC_CTRL_LVLEN_Element is ATSAMD21G18A.Bit;

   --  DMAC_CTRL_LVLEN array
   type DMAC_CTRL_LVLEN_Field_Array is array (0 .. 3)
     of DMAC_CTRL_LVLEN_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for DMAC_CTRL_LVLEN
   type DMAC_CTRL_LVLEN_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  LVLEN as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  LVLEN as an array
            Arr : DMAC_CTRL_LVLEN_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for DMAC_CTRL_LVLEN_Field use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   --  Control
   type DMAC_CTRL_Register is record
      --  Software Reset
      SWRST          : DMAC_CTRL_SWRST_Field := 16#0#;
      --  DMA Enable
      DMAENABLE      : DMAC_CTRL_DMAENABLE_Field := 16#0#;
      --  CRC Enable
      CRCENABLE      : DMAC_CTRL_CRCENABLE_Field := 16#0#;
      --  unspecified
      Reserved_3_7   : ATSAMD21G18A.UInt5 := 16#0#;
      --  Priority Level 0 Enable
      LVLEN          : DMAC_CTRL_LVLEN_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_12_15 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 16,
          Bit_Order => System.Low_Order_First;

   for DMAC_CTRL_Register use record
      SWRST          at 0 range 0 .. 0;
      DMAENABLE      at 0 range 1 .. 1;
      CRCENABLE      at 0 range 2 .. 2;
      Reserved_3_7   at 0 range 3 .. 7;
      LVLEN          at 0 range 8 .. 11;
      Reserved_12_15 at 0 range 12 .. 15;
   end record;

   --  CRC Beat Size
   type CRCCTRL_CRCBEATSIZESelect is
     (
      --  8-bit bus transfer
      Byte,
      --  16-bit bus transfer
      Hword,
      --  32-bit bus transfer
      Word)
     with Size => 2;
   for CRCCTRL_CRCBEATSIZESelect use
     (Byte => 0,
      Hword => 1,
      Word => 2);

   --  CRC Polynomial Type
   type CRCCTRL_CRCPOLYSelect is
     (
      --  CRC-16 (CRC-CCITT)
      Crc16,
      --  CRC32 (IEEE 802.3)
      Crc32)
     with Size => 2;
   for CRCCTRL_CRCPOLYSelect use
     (Crc16 => 0,
      Crc32 => 1);

   --  CRC Input Source
   type CRCCTRL_CRCSRCSelect is
     (
      --  No action
      Noact,
      --  I/O interface
      Io)
     with Size => 6;
   for CRCCTRL_CRCSRCSelect use
     (Noact => 0,
      Io => 1);

   --  CRC Control
   type DMAC_CRCCTRL_Register is record
      --  CRC Beat Size
      CRCBEATSIZE    : CRCCTRL_CRCBEATSIZESelect := ATSAMD21G18A.DMAC.Byte;
      --  CRC Polynomial Type
      CRCPOLY        : CRCCTRL_CRCPOLYSelect := ATSAMD21G18A.DMAC.Crc16;
      --  unspecified
      Reserved_4_7   : ATSAMD21G18A.UInt4 := 16#0#;
      --  CRC Input Source
      CRCSRC         : CRCCTRL_CRCSRCSelect := ATSAMD21G18A.DMAC.Noact;
      --  unspecified
      Reserved_14_15 : ATSAMD21G18A.UInt2 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 16,
          Bit_Order => System.Low_Order_First;

   for DMAC_CRCCTRL_Register use record
      CRCBEATSIZE    at 0 range 0 .. 1;
      CRCPOLY        at 0 range 2 .. 3;
      Reserved_4_7   at 0 range 4 .. 7;
      CRCSRC         at 0 range 8 .. 13;
      Reserved_14_15 at 0 range 14 .. 15;
   end record;

   subtype DMAC_CRCSTATUS_CRCBUSY_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CRCSTATUS_CRCZERO_Field is ATSAMD21G18A.Bit;

   --  CRC Status
   type DMAC_CRCSTATUS_Register is record
      --  CRC Module Busy
      CRCBUSY      : DMAC_CRCSTATUS_CRCBUSY_Field := 16#0#;
      --  Read-only. CRC Zero
      CRCZERO      : DMAC_CRCSTATUS_CRCZERO_Field := 16#0#;
      --  unspecified
      Reserved_2_7 : ATSAMD21G18A.UInt6 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CRCSTATUS_Register use record
      CRCBUSY      at 0 range 0 .. 0;
      CRCZERO      at 0 range 1 .. 1;
      Reserved_2_7 at 0 range 2 .. 7;
   end record;

   subtype DMAC_DBGCTRL_DBGRUN_Field is ATSAMD21G18A.Bit;

   --  Debug Control
   type DMAC_DBGCTRL_Register is record
      --  Debug Run
      DBGRUN       : DMAC_DBGCTRL_DBGRUN_Field := 16#0#;
      --  unspecified
      Reserved_1_7 : ATSAMD21G18A.UInt7 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_DBGCTRL_Register use record
      DBGRUN       at 0 range 0 .. 0;
      Reserved_1_7 at 0 range 1 .. 7;
   end record;

   --  Write-Back Quality of Service
   type QOSCTRL_WRBQOSSelect is
     (
      --  Background (no sensitive operation)
      Disable,
      --  Sensitive Bandwidth
      Low,
      --  Sensitive Latency
      Medium,
      --  Critical Latency
      High)
     with Size => 2;
   for QOSCTRL_WRBQOSSelect use
     (Disable => 0,
      Low => 1,
      Medium => 2,
      High => 3);

   --  Fetch Quality of Service
   type QOSCTRL_FQOSSelect is
     (
      --  Background (no sensitive operation)
      Disable,
      --  Sensitive Bandwidth
      Low,
      --  Sensitive Latency
      Medium,
      --  Critical Latency
      High)
     with Size => 2;
   for QOSCTRL_FQOSSelect use
     (Disable => 0,
      Low => 1,
      Medium => 2,
      High => 3);

   --  Data Transfer Quality of Service
   type QOSCTRL_DQOSSelect is
     (
      --  Background (no sensitive operation)
      Disable,
      --  Sensitive Bandwidth
      Low,
      --  Sensitive Latency
      Medium,
      --  Critical Latency
      High)
     with Size => 2;
   for QOSCTRL_DQOSSelect use
     (Disable => 0,
      Low => 1,
      Medium => 2,
      High => 3);

   --  QOS Control
   type DMAC_QOSCTRL_Register is record
      --  Write-Back Quality of Service
      WRBQOS       : QOSCTRL_WRBQOSSelect := ATSAMD21G18A.DMAC.Low;
      --  Fetch Quality of Service
      FQOS         : QOSCTRL_FQOSSelect := ATSAMD21G18A.DMAC.Low;
      --  Data Transfer Quality of Service
      DQOS         : QOSCTRL_DQOSSelect := ATSAMD21G18A.DMAC.Low;
      --  unspecified
      Reserved_6_7 : ATSAMD21G18A.UInt2 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_QOSCTRL_Register use record
      WRBQOS       at 0 range 0 .. 1;
      FQOS         at 0 range 2 .. 3;
      DQOS         at 0 range 4 .. 5;
      Reserved_6_7 at 0 range 6 .. 7;
   end record;

   --  DMAC_SWTRIGCTRL_SWTRIG array element
   subtype DMAC_SWTRIGCTRL_SWTRIG_Element is ATSAMD21G18A.Bit;

   --  DMAC_SWTRIGCTRL_SWTRIG array
   type DMAC_SWTRIGCTRL_SWTRIG_Field_Array is array (0 .. 11)
     of DMAC_SWTRIGCTRL_SWTRIG_Element
     with Component_Size => 1, Size => 12;

   --  Type definition for DMAC_SWTRIGCTRL_SWTRIG
   type DMAC_SWTRIGCTRL_SWTRIG_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  SWTRIG as a value
            Val : ATSAMD21G18A.UInt12;
         when True =>
            --  SWTRIG as an array
            Arr : DMAC_SWTRIGCTRL_SWTRIG_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 12;

   for DMAC_SWTRIGCTRL_SWTRIG_Field use record
      Val at 0 range 0 .. 11;
      Arr at 0 range 0 .. 11;
   end record;

   --  Software Trigger Control
   type DMAC_SWTRIGCTRL_Register is record
      --  Channel 0 Software Trigger
      SWTRIG         : DMAC_SWTRIGCTRL_SWTRIG_Field :=
                        (As_Array => False, Val => 16#0#);
      --  unspecified
      Reserved_12_31 : ATSAMD21G18A.UInt20 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_SWTRIGCTRL_Register use record
      SWTRIG         at 0 range 0 .. 11;
      Reserved_12_31 at 0 range 12 .. 31;
   end record;

   subtype DMAC_PRICTRL0_LVLPRI0_Field is ATSAMD21G18A.UInt4;
   subtype DMAC_PRICTRL0_RRLVLEN0_Field is ATSAMD21G18A.Bit;
   subtype DMAC_PRICTRL0_LVLPRI1_Field is ATSAMD21G18A.UInt4;
   subtype DMAC_PRICTRL0_RRLVLEN1_Field is ATSAMD21G18A.Bit;
   subtype DMAC_PRICTRL0_LVLPRI2_Field is ATSAMD21G18A.UInt4;
   subtype DMAC_PRICTRL0_RRLVLEN2_Field is ATSAMD21G18A.Bit;
   subtype DMAC_PRICTRL0_LVLPRI3_Field is ATSAMD21G18A.UInt4;
   subtype DMAC_PRICTRL0_RRLVLEN3_Field is ATSAMD21G18A.Bit;

   --  Priority Control 0
   type DMAC_PRICTRL0_Register is record
      --  Level 0 Channel Priority Number
      LVLPRI0        : DMAC_PRICTRL0_LVLPRI0_Field := 16#0#;
      --  unspecified
      Reserved_4_6   : ATSAMD21G18A.UInt3 := 16#0#;
      --  Level 0 Round-Robin Scheduling Enable
      RRLVLEN0       : DMAC_PRICTRL0_RRLVLEN0_Field := 16#0#;
      --  Level 1 Channel Priority Number
      LVLPRI1        : DMAC_PRICTRL0_LVLPRI1_Field := 16#0#;
      --  unspecified
      Reserved_12_14 : ATSAMD21G18A.UInt3 := 16#0#;
      --  Level 1 Round-Robin Scheduling Enable
      RRLVLEN1       : DMAC_PRICTRL0_RRLVLEN1_Field := 16#0#;
      --  Level 2 Channel Priority Number
      LVLPRI2        : DMAC_PRICTRL0_LVLPRI2_Field := 16#0#;
      --  unspecified
      Reserved_20_22 : ATSAMD21G18A.UInt3 := 16#0#;
      --  Level 2 Round-Robin Scheduling Enable
      RRLVLEN2       : DMAC_PRICTRL0_RRLVLEN2_Field := 16#0#;
      --  Level 3 Channel Priority Number
      LVLPRI3        : DMAC_PRICTRL0_LVLPRI3_Field := 16#0#;
      --  unspecified
      Reserved_28_30 : ATSAMD21G18A.UInt3 := 16#0#;
      --  Level 3 Round-Robin Scheduling Enable
      RRLVLEN3       : DMAC_PRICTRL0_RRLVLEN3_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_PRICTRL0_Register use record
      LVLPRI0        at 0 range 0 .. 3;
      Reserved_4_6   at 0 range 4 .. 6;
      RRLVLEN0       at 0 range 7 .. 7;
      LVLPRI1        at 0 range 8 .. 11;
      Reserved_12_14 at 0 range 12 .. 14;
      RRLVLEN1       at 0 range 15 .. 15;
      LVLPRI2        at 0 range 16 .. 19;
      Reserved_20_22 at 0 range 20 .. 22;
      RRLVLEN2       at 0 range 23 .. 23;
      LVLPRI3        at 0 range 24 .. 27;
      Reserved_28_30 at 0 range 28 .. 30;
      RRLVLEN3       at 0 range 31 .. 31;
   end record;

   subtype DMAC_INTPEND_ID_Field is ATSAMD21G18A.UInt4;
   subtype DMAC_INTPEND_TERR_Field is ATSAMD21G18A.Bit;
   subtype DMAC_INTPEND_TCMPL_Field is ATSAMD21G18A.Bit;
   subtype DMAC_INTPEND_SUSP_Field is ATSAMD21G18A.Bit;
   subtype DMAC_INTPEND_FERR_Field is ATSAMD21G18A.Bit;
   subtype DMAC_INTPEND_BUSY_Field is ATSAMD21G18A.Bit;
   subtype DMAC_INTPEND_PEND_Field is ATSAMD21G18A.Bit;

   --  Interrupt Pending
   type DMAC_INTPEND_Register is record
      --  Channel ID
      ID             : DMAC_INTPEND_ID_Field := 16#0#;
      --  unspecified
      Reserved_4_7   : ATSAMD21G18A.UInt4 := 16#0#;
      --  Transfer Error
      TERR           : DMAC_INTPEND_TERR_Field := 16#0#;
      --  Transfer Complete
      TCMPL          : DMAC_INTPEND_TCMPL_Field := 16#0#;
      --  Channel Suspend
      SUSP           : DMAC_INTPEND_SUSP_Field := 16#0#;
      --  unspecified
      Reserved_11_12 : ATSAMD21G18A.UInt2 := 16#0#;
      --  Read-only. Fetch Error
      FERR           : DMAC_INTPEND_FERR_Field := 16#0#;
      --  Read-only. Busy
      BUSY           : DMAC_INTPEND_BUSY_Field := 16#0#;
      --  Read-only. Pending
      PEND           : DMAC_INTPEND_PEND_Field := 16#0#;
   end record
     with Volatile_Full_Access, Size => 16,
          Bit_Order => System.Low_Order_First;

   for DMAC_INTPEND_Register use record
      ID             at 0 range 0 .. 3;
      Reserved_4_7   at 0 range 4 .. 7;
      TERR           at 0 range 8 .. 8;
      TCMPL          at 0 range 9 .. 9;
      SUSP           at 0 range 10 .. 10;
      Reserved_11_12 at 0 range 11 .. 12;
      FERR           at 0 range 13 .. 13;
      BUSY           at 0 range 14 .. 14;
      PEND           at 0 range 15 .. 15;
   end record;

   --  DMAC_INTSTATUS_CHINT array element
   subtype DMAC_INTSTATUS_CHINT_Element is ATSAMD21G18A.Bit;

   --  DMAC_INTSTATUS_CHINT array
   type DMAC_INTSTATUS_CHINT_Field_Array is array (0 .. 11)
     of DMAC_INTSTATUS_CHINT_Element
     with Component_Size => 1, Size => 12;

   --  Type definition for DMAC_INTSTATUS_CHINT
   type DMAC_INTSTATUS_CHINT_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  CHINT as a value
            Val : ATSAMD21G18A.UInt12;
         when True =>
            --  CHINT as an array
            Arr : DMAC_INTSTATUS_CHINT_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 12;

   for DMAC_INTSTATUS_CHINT_Field use record
      Val at 0 range 0 .. 11;
      Arr at 0 range 0 .. 11;
   end record;

   --  Interrupt Status
   type DMAC_INTSTATUS_Register is record
      --  Read-only. Channel 0 Pending Interrupt
      CHINT          : DMAC_INTSTATUS_CHINT_Field;
      --  unspecified
      Reserved_12_31 : ATSAMD21G18A.UInt20;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_INTSTATUS_Register use record
      CHINT          at 0 range 0 .. 11;
      Reserved_12_31 at 0 range 12 .. 31;
   end record;

   --  DMAC_BUSYCH_BUSYCH array element
   subtype DMAC_BUSYCH_BUSYCH_Element is ATSAMD21G18A.Bit;

   --  DMAC_BUSYCH_BUSYCH array
   type DMAC_BUSYCH_BUSYCH_Field_Array is array (0 .. 11)
     of DMAC_BUSYCH_BUSYCH_Element
     with Component_Size => 1, Size => 12;

   --  Type definition for DMAC_BUSYCH_BUSYCH
   type DMAC_BUSYCH_BUSYCH_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  BUSYCH as a value
            Val : ATSAMD21G18A.UInt12;
         when True =>
            --  BUSYCH as an array
            Arr : DMAC_BUSYCH_BUSYCH_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 12;

   for DMAC_BUSYCH_BUSYCH_Field use record
      Val at 0 range 0 .. 11;
      Arr at 0 range 0 .. 11;
   end record;

   --  Busy Channels
   type DMAC_BUSYCH_Register is record
      --  Read-only. Busy Channel 0
      BUSYCH         : DMAC_BUSYCH_BUSYCH_Field;
      --  unspecified
      Reserved_12_31 : ATSAMD21G18A.UInt20;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_BUSYCH_Register use record
      BUSYCH         at 0 range 0 .. 11;
      Reserved_12_31 at 0 range 12 .. 31;
   end record;

   --  DMAC_PENDCH_PENDCH array element
   subtype DMAC_PENDCH_PENDCH_Element is ATSAMD21G18A.Bit;

   --  DMAC_PENDCH_PENDCH array
   type DMAC_PENDCH_PENDCH_Field_Array is array (0 .. 11)
     of DMAC_PENDCH_PENDCH_Element
     with Component_Size => 1, Size => 12;

   --  Type definition for DMAC_PENDCH_PENDCH
   type DMAC_PENDCH_PENDCH_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  PENDCH as a value
            Val : ATSAMD21G18A.UInt12;
         when True =>
            --  PENDCH as an array
            Arr : DMAC_PENDCH_PENDCH_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 12;

   for DMAC_PENDCH_PENDCH_Field use record
      Val at 0 range 0 .. 11;
      Arr at 0 range 0 .. 11;
   end record;

   --  Pending Channels
   type DMAC_PENDCH_Register is record
      --  Read-only. Pending Channel 0
      PENDCH         : DMAC_PENDCH_PENDCH_Field;
      --  unspecified
      Reserved_12_31 : ATSAMD21G18A.UInt20;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_PENDCH_Register use record
      PENDCH         at 0 range 0 .. 11;
      Reserved_12_31 at 0 range 12 .. 31;
   end record;

   --  DMAC_ACTIVE_LVLEX array element
   subtype DMAC_ACTIVE_LVLEX_Element is ATSAMD21G18A.Bit;

   --  DMAC_ACTIVE_LVLEX array
   type DMAC_ACTIVE_LVLEX_Field_Array is array (0 .. 3)
     of DMAC_ACTIVE_LVLEX_Element
     with Component_Size => 1, Size => 4;

   --  Type definition for DMAC_ACTIVE_LVLEX
   type DMAC_ACTIVE_LVLEX_Field
     (As_Array : Boolean := False)
   is record
      case As_Array is
         when False =>
            --  LVLEX as a value
            Val : ATSAMD21G18A.UInt4;
         when True =>
            --  LVLEX as an array
            Arr : DMAC_ACTIVE_LVLEX_Field_Array;
      end case;
   end record
     with Unchecked_Union, Size => 4;

   for DMAC_ACTIVE_LVLEX_Field use record
      Val at 0 range 0 .. 3;
      Arr at 0 range 0 .. 3;
   end record;

   subtype DMAC_ACTIVE_ID_Field is ATSAMD21G18A.UInt5;
   subtype DMAC_ACTIVE_ABUSY_Field is ATSAMD21G18A.Bit;
   subtype DMAC_ACTIVE_BTCNT_Field is ATSAMD21G18A.UInt16;

   --  Active Channel and Levels
   type DMAC_ACTIVE_Register is record
      --  Read-only. Level 0 Channel Trigger Request Executing
      LVLEX          : DMAC_ACTIVE_LVLEX_Field;
      --  unspecified
      Reserved_4_7   : ATSAMD21G18A.UInt4;
      --  Read-only. Active Channel ID
      ID             : DMAC_ACTIVE_ID_Field;
      --  unspecified
      Reserved_13_14 : ATSAMD21G18A.UInt2;
      --  Read-only. Active Channel Busy
      ABUSY          : DMAC_ACTIVE_ABUSY_Field;
      --  Read-only. Active Channel Block Transfer Count
      BTCNT          : DMAC_ACTIVE_BTCNT_Field;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_ACTIVE_Register use record
      LVLEX          at 0 range 0 .. 3;
      Reserved_4_7   at 0 range 4 .. 7;
      ID             at 0 range 8 .. 12;
      Reserved_13_14 at 0 range 13 .. 14;
      ABUSY          at 0 range 15 .. 15;
      BTCNT          at 0 range 16 .. 31;
   end record;

   subtype DMAC_CHID_ID_Field is ATSAMD21G18A.UInt4;

   --  Channel ID
   type DMAC_CHID_Register is record
      --  Channel ID
      ID           : DMAC_CHID_ID_Field := 16#0#;
      --  unspecified
      Reserved_4_7 : ATSAMD21G18A.UInt4 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CHID_Register use record
      ID           at 0 range 0 .. 3;
      Reserved_4_7 at 0 range 4 .. 7;
   end record;

   subtype DMAC_CHCTRLA_SWRST_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHCTRLA_ENABLE_Field is ATSAMD21G18A.Bit;

   --  Channel Control A
   type DMAC_CHCTRLA_Register is record
      --  Channel Software Reset
      SWRST        : DMAC_CHCTRLA_SWRST_Field := 16#0#;
      --  Channel Enable
      ENABLE       : DMAC_CHCTRLA_ENABLE_Field := 16#0#;
      --  unspecified
      Reserved_2_7 : ATSAMD21G18A.UInt6 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CHCTRLA_Register use record
      SWRST        at 0 range 0 .. 0;
      ENABLE       at 0 range 1 .. 1;
      Reserved_2_7 at 0 range 2 .. 7;
   end record;

   --  Event Input Action
   type CHCTRLB_EVACTSelect is
     (
      --  No action
      Noact,
      --  Transfer and periodic transfer trigger
      Trig,
      --  Conditional transfer trigger
      Ctrig,
      --  Conditional block transfer
      Cblock,
      --  Channel suspend operation
      Suspend,
      --  Channel resume operation
      Resume,
      --  Skip next block suspend action
      Sskip)
     with Size => 3;
   for CHCTRLB_EVACTSelect use
     (Noact => 0,
      Trig => 1,
      Ctrig => 2,
      Cblock => 3,
      Suspend => 4,
      Resume => 5,
      Sskip => 6);

   subtype DMAC_CHCTRLB_EVIE_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHCTRLB_EVOE_Field is ATSAMD21G18A.Bit;

   --  Channel Arbitration Level
   type CHCTRLB_LVLSelect is
     (
      --  Channel Priority Level 0
      Lvl0,
      --  Channel Priority Level 1
      Lvl1,
      --  Channel Priority Level 2
      Lvl2,
      --  Channel Priority Level 3
      Lvl3)
     with Size => 2;
   for CHCTRLB_LVLSelect use
     (Lvl0 => 0,
      Lvl1 => 1,
      Lvl2 => 2,
      Lvl3 => 3);

   --  Trigger Source
   type CHCTRLB_TRIGSRCSelect is
     (
      --  Only software/event triggers
      Disable)
     with Size => 6;
   for CHCTRLB_TRIGSRCSelect use
     (Disable => 0);

   --  Trigger Action
   type CHCTRLB_TRIGACTSelect is
     (
      --  One trigger required for each block transfer
      Block,
      --  One trigger required for each beat transfer
      Beat,
      --  One trigger required for each transaction
      Transaction)
     with Size => 2;
   for CHCTRLB_TRIGACTSelect use
     (Block => 0,
      Beat => 2,
      Transaction => 3);

   --  Software Command
   type CHCTRLB_CMDSelect is
     (
      --  No action
      Noact,
      --  Channel suspend operation
      Suspend,
      --  Channel resume operation
      Resume)
     with Size => 2;
   for CHCTRLB_CMDSelect use
     (Noact => 0,
      Suspend => 1,
      Resume => 2);

   --  Channel Control B
   type DMAC_CHCTRLB_Register is record
      --  Event Input Action
      EVACT          : CHCTRLB_EVACTSelect := ATSAMD21G18A.DMAC.Noact;
      --  Channel Event Input Enable
      EVIE           : DMAC_CHCTRLB_EVIE_Field := 16#0#;
      --  Channel Event Output Enable
      EVOE           : DMAC_CHCTRLB_EVOE_Field := 16#0#;
      --  Channel Arbitration Level
      LVL            : CHCTRLB_LVLSelect := ATSAMD21G18A.DMAC.Lvl0;
      --  unspecified
      Reserved_7_7   : ATSAMD21G18A.Bit := 16#0#;
      --  Trigger Source
      TRIGSRC        : CHCTRLB_TRIGSRCSelect := ATSAMD21G18A.DMAC.Disable;
      --  unspecified
      Reserved_14_21 : ATSAMD21G18A.Byte := 16#0#;
      --  Trigger Action
      TRIGACT        : CHCTRLB_TRIGACTSelect := ATSAMD21G18A.DMAC.Block;
      --  Software Command
      CMD            : CHCTRLB_CMDSelect := ATSAMD21G18A.DMAC.Noact;
      --  unspecified
      Reserved_26_31 : ATSAMD21G18A.UInt6 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 32,
          Bit_Order => System.Low_Order_First;

   for DMAC_CHCTRLB_Register use record
      EVACT          at 0 range 0 .. 2;
      EVIE           at 0 range 3 .. 3;
      EVOE           at 0 range 4 .. 4;
      LVL            at 0 range 5 .. 6;
      Reserved_7_7   at 0 range 7 .. 7;
      TRIGSRC        at 0 range 8 .. 13;
      Reserved_14_21 at 0 range 14 .. 21;
      TRIGACT        at 0 range 22 .. 23;
      CMD            at 0 range 24 .. 25;
      Reserved_26_31 at 0 range 26 .. 31;
   end record;

   subtype DMAC_CHINTENCLR_TERR_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHINTENCLR_TCMPL_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHINTENCLR_SUSP_Field is ATSAMD21G18A.Bit;

   --  Channel Interrupt Enable Clear
   type DMAC_CHINTENCLR_Register is record
      --  Channel Transfer Error Interrupt Enable
      TERR         : DMAC_CHINTENCLR_TERR_Field := 16#0#;
      --  Channel Transfer Complete Interrupt Enable
      TCMPL        : DMAC_CHINTENCLR_TCMPL_Field := 16#0#;
      --  Channel Suspend Interrupt Enable
      SUSP         : DMAC_CHINTENCLR_SUSP_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CHINTENCLR_Register use record
      TERR         at 0 range 0 .. 0;
      TCMPL        at 0 range 1 .. 1;
      SUSP         at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DMAC_CHINTENSET_TERR_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHINTENSET_TCMPL_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHINTENSET_SUSP_Field is ATSAMD21G18A.Bit;

   --  Channel Interrupt Enable Set
   type DMAC_CHINTENSET_Register is record
      --  Channel Transfer Error Interrupt Enable
      TERR         : DMAC_CHINTENSET_TERR_Field := 16#0#;
      --  Channel Transfer Complete Interrupt Enable
      TCMPL        : DMAC_CHINTENSET_TCMPL_Field := 16#0#;
      --  Channel Suspend Interrupt Enable
      SUSP         : DMAC_CHINTENSET_SUSP_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CHINTENSET_Register use record
      TERR         at 0 range 0 .. 0;
      TCMPL        at 0 range 1 .. 1;
      SUSP         at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DMAC_CHINTFLAG_TERR_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHINTFLAG_TCMPL_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHINTFLAG_SUSP_Field is ATSAMD21G18A.Bit;

   --  Channel Interrupt Flag Status and Clear
   type DMAC_CHINTFLAG_Register is record
      --  Channel Transfer Error
      TERR         : DMAC_CHINTFLAG_TERR_Field := 16#0#;
      --  Channel Transfer Complete
      TCMPL        : DMAC_CHINTFLAG_TCMPL_Field := 16#0#;
      --  Channel Suspend
      SUSP         : DMAC_CHINTFLAG_SUSP_Field := 16#0#;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5 := 16#0#;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CHINTFLAG_Register use record
      TERR         at 0 range 0 .. 0;
      TCMPL        at 0 range 1 .. 1;
      SUSP         at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   subtype DMAC_CHSTATUS_PEND_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHSTATUS_BUSY_Field is ATSAMD21G18A.Bit;
   subtype DMAC_CHSTATUS_FERR_Field is ATSAMD21G18A.Bit;

   --  Channel Status
   type DMAC_CHSTATUS_Register is record
      --  Read-only. Channel Pending
      PEND         : DMAC_CHSTATUS_PEND_Field;
      --  Read-only. Channel Busy
      BUSY         : DMAC_CHSTATUS_BUSY_Field;
      --  Read-only. Channel Fetch Error
      FERR         : DMAC_CHSTATUS_FERR_Field;
      --  unspecified
      Reserved_3_7 : ATSAMD21G18A.UInt5;
   end record
     with Volatile_Full_Access, Size => 8, Bit_Order => System.Low_Order_First;

   for DMAC_CHSTATUS_Register use record
      PEND         at 0 range 0 .. 0;
      BUSY         at 0 range 1 .. 1;
      FERR         at 0 range 2 .. 2;
      Reserved_3_7 at 0 range 3 .. 7;
   end record;

   -----------------
   -- Peripherals --
   -----------------

   --  Direct Memory Access Controller
   type DMAC_Peripheral is record
      --  Control
      CTRL       : aliased DMAC_CTRL_Register;
      --  CRC Control
      CRCCTRL    : aliased DMAC_CRCCTRL_Register;
      --  CRC Data Input
      CRCDATAIN  : aliased ATSAMD21G18A.UInt32;
      --  CRC Checksum
      CRCCHKSUM  : aliased ATSAMD21G18A.UInt32;
      --  CRC Status
      CRCSTATUS  : aliased DMAC_CRCSTATUS_Register;
      --  Debug Control
      DBGCTRL    : aliased DMAC_DBGCTRL_Register;
      --  QOS Control
      QOSCTRL    : aliased DMAC_QOSCTRL_Register;
      --  Software Trigger Control
      SWTRIGCTRL : aliased DMAC_SWTRIGCTRL_Register;
      --  Priority Control 0
      PRICTRL0   : aliased DMAC_PRICTRL0_Register;
      --  Interrupt Pending
      INTPEND    : aliased DMAC_INTPEND_Register;
      --  Interrupt Status
      INTSTATUS  : aliased DMAC_INTSTATUS_Register;
      --  Busy Channels
      BUSYCH     : aliased DMAC_BUSYCH_Register;
      --  Pending Channels
      PENDCH     : aliased DMAC_PENDCH_Register;
      --  Active Channel and Levels
      ACTIVE     : aliased DMAC_ACTIVE_Register;
      --  Descriptor Memory Section Base Address
      BASEADDR   : aliased ATSAMD21G18A.UInt32;
      --  Write-Back Memory Section Base Address
      WRBADDR    : aliased ATSAMD21G18A.UInt32;
      --  Channel ID
      CHID       : aliased DMAC_CHID_Register;
      --  Channel Control A
      CHCTRLA    : aliased DMAC_CHCTRLA_Register;
      --  Channel Control B
      CHCTRLB    : aliased DMAC_CHCTRLB_Register;
      --  Channel Interrupt Enable Clear
      CHINTENCLR : aliased DMAC_CHINTENCLR_Register;
      --  Channel Interrupt Enable Set
      CHINTENSET : aliased DMAC_CHINTENSET_Register;
      --  Channel Interrupt Flag Status and Clear
      CHINTFLAG  : aliased DMAC_CHINTFLAG_Register;
      --  Channel Status
      CHSTATUS   : aliased DMAC_CHSTATUS_Register;
   end record
     with Volatile;

   for DMAC_Peripheral use record
      CTRL       at 16#0# range 0 .. 15;
      CRCCTRL    at 16#2# range 0 .. 15;
      CRCDATAIN  at 16#4# range 0 .. 31;
      CRCCHKSUM  at 16#8# range 0 .. 31;
      CRCSTATUS  at 16#C# range 0 .. 7;
      DBGCTRL    at 16#D# range 0 .. 7;
      QOSCTRL    at 16#E# range 0 .. 7;
      SWTRIGCTRL at 16#10# range 0 .. 31;
      PRICTRL0   at 16#14# range 0 .. 31;
      INTPEND    at 16#20# range 0 .. 15;
      INTSTATUS  at 16#24# range 0 .. 31;
      BUSYCH     at 16#28# range 0 .. 31;
      PENDCH     at 16#2C# range 0 .. 31;
      ACTIVE     at 16#30# range 0 .. 31;
      BASEADDR   at 16#34# range 0 .. 31;
      WRBADDR    at 16#38# range 0 .. 31;
      CHID       at 16#3F# range 0 .. 7;
      CHCTRLA    at 16#40# range 0 .. 7;
      CHCTRLB    at 16#44# range 0 .. 31;
      CHINTENCLR at 16#4C# range 0 .. 7;
      CHINTENSET at 16#4D# range 0 .. 7;
      CHINTFLAG  at 16#4E# range 0 .. 7;
      CHSTATUS   at 16#4F# range 0 .. 7;
   end record;

   --  Direct Memory Access Controller
   DMAC_Periph : aliased DMAC_Peripheral
     with Import, Address => System'To_Address (16#41004800#);

end ATSAMD21G18A.DMAC;
