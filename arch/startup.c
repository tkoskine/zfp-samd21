/* This file is from
 * https://github.com/vsergeev/arm-bmw-sw/blob/master/src/system/startup.c
 *
 * Copyright (c) 2014 vsergeev / Ivan (Vanya) A. Sergeev
 *
 * Distributed under MIT license.
 */
typedef unsigned long uint32_t;
typedef unsigned char uint8_t;

extern uint32_t _end_stack;
extern uint32_t _start_data_flash;
extern uint32_t _start_data;
extern uint32_t _end_data;
extern uint32_t _start_bss;
extern uint32_t _end_bss;

/* Application main() called in reset handler */
extern int main(void);

#define WEAK_ALIAS(x) __attribute__ ((weak, alias(#x)))
#define WEAK __attribute__ ((weak))

void dummy_handler(void) { }
void Reset_Handler(void);

WEAK void NMI_Handler(void) { while(1); }
WEAK void HardFault_Handler(void) { while(1); }
WEAK void SVC_Handler(void) { while(1); };
WEAK void PendSV_Handler(void) { while(1); };
WEAK void SysTick_Handler(void) { while(1); };
WEAK void PM_Handler(void) { while(1); };
WEAK void SYSCTRL_Handler(void) { while(1); };
WEAK void WDT_Handler(void) { while(1); };
WEAK void RTC_Handler(void) { while(1); };
WEAK void EIC_Handler(void) { while(1); };
WEAK void NVMCTRL_Handler(void) { while(1); };
WEAK void DMAC_Handler(void) { while(1); };
WEAK void USB_Handler(void) { while(1); };
WEAK void EVSYS_Handler(void) { while(1); };
WEAK void SERCOM0_Handler(void) { while(1); };
WEAK void SERCOM1_Handler(void) { while(1); };
WEAK void SERCOM2_Handler(void) { while(1); };
WEAK void SERCOM3_Handler(void) { while(1); };
WEAK void SERCOM4_Handler(void) { while(1); };
WEAK void SERCOM5_Handler(void) { while(1); };
WEAK void TCC0_Handler(void) { while(1); };
WEAK void TCC1_Handler(void) { while(1); };
WEAK void TCC2_Handler(void) { while(1); };
WEAK void TC3_Handler(void) { while(1); };
WEAK void TC4_Handler(void) { while(1); };
WEAK void TC5_Handler(void) { while(1); };
WEAK void ADC_Handler(void) { while(1); };
WEAK void AC_Handler(void) { while(1); };
WEAK void DAC_Handler(void) { while(1); };
WEAK void I2S_Handler(void) { while(1); };

void *vector_table[] __attribute__ ((section(".vectors"))) = {
  &_end_stack,
  Reset_Handler,
  NMI_Handler,
  HardFault_Handler,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  SVC_Handler,
  0,
  0,
  PendSV_Handler,
  SysTick_Handler,
  PM_Handler,
  SYSCTRL_Handler,
  WDT_Handler,
  RTC_Handler,
  EIC_Handler,
  NVMCTRL_Handler,
  DMAC_Handler,
  USB_Handler,
  EVSYS_Handler,
  SERCOM0_Handler,
  SERCOM1_Handler,
  SERCOM2_Handler,
  SERCOM3_Handler,
  SERCOM4_Handler,
  SERCOM5_Handler,
  TCC0_Handler,
  TCC1_Handler,
  TCC2_Handler,
  TC3_Handler,
  TC4_Handler,
  TC5_Handler,
  dummy_handler,
  dummy_handler,
  ADC_Handler,
  AC_Handler,
  DAC_Handler,
  dummy_handler,
  I2S_Handler,
};

__attribute__ ((naked))
void Reset_Handler(void) {
    uint8_t *src, *dst;

    /* Copy with byte pointers to obviate unaligned access problems */

    /* Copy data section from Flash to RAM */
    src = (uint8_t *)&_start_data_flash;
    dst = (uint8_t *)&_start_data;
    while (dst < (uint8_t *)&_end_data)
        *dst++ = *src++;

    /* Clear the bss section */
    dst = (uint8_t *)&_start_bss;
    while (dst < (uint8_t *)&_end_bss)
        *dst++ = 0;

    main();
}


