# Ada Runtime System (RTS; ZFP Profile) for Atmel SAMD21

## Usage

Rebuilding the RTS is not required before use.

1. Copy / git submodule the repository to your project

2. `gnatmake --RTS=$PWD/zfp-samd21` or add to your gprbuild project
   file:
~~~ Ada
package Builder is
    for Switches ("Ada") use ("--RTS=" & Project'project_dir & zfp-samd21);
end Builder;
~~~

## Building

1. Download / install [GNAT GPL from AdaCore]() (ARM-ELF)

2. `gprbuild runtime_build.gpr`

Note:
Ada code is mostly copied from https://github.com/nocko/zfp-nrf51

SAMD21 startup code is from
https://github.com/vsergeev/arm-bmw-sw/blob/master/src/system/startup.c


Tero Koskinen <tero.koskinen@iki.fi>
